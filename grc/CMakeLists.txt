# Copyright 2011 Free Software Foundation, Inc.
#
# This file is part of GNU Radio
#
# GNU Radio is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# GNU Radio is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNU Radio; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
install(FILES
    marmote3_polyphase_synt_filter_ccf.xml
    marmote3_polyphase_chan_filter_ccf.xml
    marmote3_vector_fir_filter_xxx.xml
    marmote3_periodic_multiply_const_cc.xml
    marmote3_subcarrier_allocator.xml
    marmote3_transpose_vxx.xml
    marmote3_block_sum_vxx.xml
    marmote3_chirp_frame_timing.xml
    marmote3_subcarrier_remapper.xml
    marmote3_polyphase_rotator_vcc.xml
    marmote3_packet_preamb_equalizer.xml
    marmote3_packet_freq_corrector.xml
    marmote3_packet_symb_mod.xml
    marmote3_packet_symb_demod.xml
    marmote3_packet_preamb_insert.xml
    marmote3_packet_ra_encoder.xml
    marmote3_packet_ra_decoder.xml
    marmote3_packet_flow_splitter.xml
    marmote3_message_to_packet.xml
    marmote3_packet_to_message.xml
    marmote3_keep_large_segments.xml
    marmote3_packet_flow_merger.xml
    marmote3_fast_dc_blocker.xml
    marmote3_zmq_packet_sink.xml
    marmote3_fixed_tuntap_pdu.xml
    marmote3_packet_debug.xml
    marmote3_modem_bert_sender.xml
    marmote3_test_message_source.xml
    marmote3_test_message_sink.xml
    marmote3_modem_receiver.xml
    marmote3_modem_bert_receiver.xml
    marmote3_header_compressor.xml
    marmote3_header_decompressor.xml
    marmote3_packet_rep_encoder.xml
    marmote3_packet_rep_decoder.xml
    marmote3_scenario_emulator_simple.xml
    marmote3_packet_ampl_corrector.xml
    marmote3_peak_probe.xml
    marmote3_hardware_agc.xml
    marmote3_subcarrier_serializer2.xml
    marmote3_vector_peak_probe.xml
    marmote3_packet_preamb_freqcorr.xml
    marmote3_packet_preamb_freqcorr2.xml
    marmote3_spectrum_inverter.xml 
    marmote3_set_core_affinities.xml
    marmote3_modem_monitor.xml 
    marmote3_modem_control.xml
    marmote3_modem_sender2.xml
    marmote3_radar_detector.xml
    marmote3_fft_and_magnitude.xml DESTINATION share/gnuradio/grc/blocks
)
