#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Test Header Compression
# Generated: Wed Jan 16 23:16:10 2019
##################################################

from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import marmote3


class test_header_compression(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Test Header Compression")

        ##################################################
        # Variables
        ##################################################
        self.modem_monitor = marmote3.modem_monitor(1, "tcp://127.0.0.1:7557", 1, "tcp://127.0.0.1:7555")

        ##################################################
        # Blocks
        ##################################################
        self.msg_connect((self.modem_monitor, 'unused'), (self.modem_monitor, 'unused'))
        self.marmote3_test_message_source_0 = marmote3.test_message_source(8, 1000, 3, 100000, 200, 0, 101, 102)
        self.marmote3_test_message_source_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_test_message_sink_0 = marmote3.test_message_sink(3)
        self.marmote3_test_message_sink_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_header_decompressor_0 = marmote3.header_decompressor(3, True)
        self.marmote3_header_decompressor_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_header_compressor_0 = marmote3.header_compressor(3, True)
        self.marmote3_header_compressor_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.marmote3_header_compressor_0, 'out'), (self.marmote3_header_decompressor_0, 'in'))
        self.msg_connect((self.marmote3_header_decompressor_0, 'out'), (self.marmote3_test_message_sink_0, 'in'))
        self.msg_connect((self.marmote3_test_message_source_0, 'out'), (self.marmote3_header_compressor_0, 'in'))

    def get_variable_marmote3_modem_monitor_0(self):
        return self.variable_marmote3_modem_monitor_0

    def set_variable_marmote3_modem_monitor_0(self, variable_marmote3_modem_monitor_0):
        self.variable_marmote3_modem_monitor_0 = variable_marmote3_modem_monitor_0


def main(top_block_cls=test_header_compression, options=None):

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
