#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Test Modem Arq
# Generated: Sat Jul 20 18:38:26 2019
##################################################

from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import marmote3


class test_modem_arq(gr.top_block):

    def __init__(self):
        gr.top_block.__init__(self, "Test Modem Arq")

        ##################################################
        # Variables
        ##################################################
        self.modem_monitor = marmote3.modem_monitor(1, "tcp://127.0.0.1:7557", 0, "tcp://127.0.0.1:7555")

        ##################################################
        # Blocks
        ##################################################
        self.msg_connect((self.modem_monitor, 'unused'), (self.modem_monitor, 'unused'))
        self.marmote3_test_message_source_1 = marmote3.test_message_source(1200, 1200, 4, 2000000, 200, 107, 102, 101)
        self.marmote3_test_message_source_1.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_test_message_source_0 = marmote3.test_message_source(1200, 1200, 4, 2000000, 200, 51, 101, 102)
        self.marmote3_test_message_source_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_test_message_sink_1 = marmote3.test_message_sink(4)
        self.marmote3_test_message_sink_1.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_test_message_sink_0 = marmote3.test_message_sink(4)
        self.marmote3_test_message_sink_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_modem_sender2_1 = marmote3.modem_sender2(1400, 2*1400, 2*1400, 20000, 1, 2.0, -1, True, 4, 102, (), False, True)
        self.marmote3_modem_sender2_1.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_modem_sender2_0 = marmote3.modem_sender2(1400, 2*1400, 2*1400, 20000, 1, 2.0, -1, True, 4, 101, (), False, True)
        self.marmote3_modem_sender2_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_modem_receiver_1 = marmote3.modem_receiver(4, getattr(self, 'marmote3_modem_sender2_0', marmote3.modem_sender2_sptr()))
        self.marmote3_modem_receiver_1.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_modem_receiver_0 = marmote3.modem_receiver(4, getattr(self, 'marmote3_modem_sender2_1', marmote3.modem_sender2_sptr()))
        self.marmote3_modem_receiver_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_header_decompressor_1 = marmote3.header_decompressor(4, False)
        self.marmote3_header_decompressor_1.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_header_decompressor_0 = marmote3.header_decompressor(4, False)
        self.marmote3_header_decompressor_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_header_compressor_1 = marmote3.header_compressor(4, False)
        self.marmote3_header_compressor_1.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_header_compressor_0 = marmote3.header_compressor(4, False)
        self.marmote3_header_compressor_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.blocks_throttle_1 = blocks.throttle(gr.sizeof_char*1, 2e5,True)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_char*1, 2e5,True)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.marmote3_header_compressor_0, 'out'), (self.marmote3_modem_sender2_0, 'in'))
        self.msg_connect((self.marmote3_header_compressor_1, 'out'), (self.marmote3_modem_sender2_1, 'in'))
        self.msg_connect((self.marmote3_header_decompressor_0, 'out'), (self.marmote3_test_message_sink_0, 'in'))
        self.msg_connect((self.marmote3_header_decompressor_1, 'out'), (self.marmote3_test_message_sink_1, 'in'))
        self.msg_connect((self.marmote3_modem_receiver_0, 'out'), (self.marmote3_header_decompressor_0, 'in'))
        self.msg_connect((self.marmote3_modem_receiver_1, 'out'), (self.marmote3_header_decompressor_1, 'in'))
        self.msg_connect((self.marmote3_test_message_source_0, 'out'), (self.marmote3_header_compressor_0, 'in'))
        self.msg_connect((self.marmote3_test_message_source_1, 'out'), (self.marmote3_header_compressor_1, 'in'))
        self.connect((self.blocks_throttle_0, 0), (self.marmote3_modem_receiver_0, 0))
        self.connect((self.blocks_throttle_1, 0), (self.marmote3_modem_receiver_1, 0))
        self.connect((self.marmote3_modem_sender2_0, 0), (self.blocks_throttle_0, 0))
        self.connect((self.marmote3_modem_sender2_1, 0), (self.blocks_throttle_1, 0))

    def get_variable_marmote3_modem_monitor_0(self):
        return self.variable_marmote3_modem_monitor_0

    def set_variable_marmote3_modem_monitor_0(self, variable_marmote3_modem_monitor_0):
        self.variable_marmote3_modem_monitor_0 = variable_marmote3_modem_monitor_0


def main(top_block_cls=test_modem_arq, options=None):

    tb = top_block_cls()
    tb.start()
    try:
        raw_input('Press Enter to quit: ')
    except EOFError:
        pass
    tb.stop()
    tb.wait()


if __name__ == '__main__':
    main()
