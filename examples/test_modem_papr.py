#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Test Modem Papr
# Generated: Tue Sep 24 22:48:51 2019
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt4 import Qt
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import fft
from gnuradio import gr
from gnuradio import qtgui
from gnuradio.eng_option import eng_option
from gnuradio.fft import window
from gnuradio.filter import firdes
from optparse import OptionParser
import marmote3
import numpy as np
import sip
import sys
from gnuradio import qtgui


class test_modem_papr(gr.top_block, Qt.QWidget):

    def __init__(self):
        gr.top_block.__init__(self, "Test Modem Papr")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Test Modem Papr")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "test_modem_papr")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())


        ##################################################
        # Variables
        ##################################################
        self.num_channels = num_channels = 32
        self.max_data_len = max_data_len = 1000
        self.fbmc_fft_len = fbmc_fft_len = int(num_channels * 5 / 4)
        self.chirp_len = chirp_len = 128
        self.subcarrier_map = subcarrier_map = range(0, num_channels/2) + range(fbmc_fft_len-num_channels/2, fbmc_fft_len)
        self.padding_len = padding_len = 6
        self.max_symb_len = max_symb_len = marmote3.get_mcscheme(0).get_symb_len(max_data_len)
        self.fbmc_tx_stride = fbmc_tx_stride = {16: 22, 20: 28, 24: 34, 32: 46, 40: 58}[num_channels]
        self.chirp = chirp = marmote3.get_chirp_taps(chirp_len)
        self.modem_monitor = marmote3.modem_monitor(1, "tcp://127.0.0.1:7557", 3, "tcp://127.0.0.1:7555")
        self.subcarrier_inv = subcarrier_inv = [subcarrier_map.index(x) if x in subcarrier_map else -1 for x in range(fbmc_fft_len)]
        self.samp_rate = samp_rate = 25000000/10
        self.max_frame_len = max_frame_len = 40 + padding_len + chirp_len + padding_len + max_symb_len
        self.max_code_len = max_code_len = marmote3.get_mcscheme(0).get_code_len(max_data_len)
        self.fbmc_tx_taps = fbmc_tx_taps = marmote3.get_fbmc_taps8(fbmc_fft_len, fbmc_tx_stride, "tx")
        self.dc_chirp = dc_chirp = chirp + np.ones(chirp_len) * 0.5
        self.chirp_fft_len = chirp_fft_len = 64
        self.active_channels = active_channels = [1,10]

        ##################################################
        # Blocks
        ##################################################
        self.msg_connect((self.modem_monitor, 'unused'), (self.modem_monitor, 'unused'))
        self.qtgui_number_sink_0_0 = qtgui.number_sink(
            gr.sizeof_float,
            0,
            qtgui.NUM_GRAPH_HORIZ,
            1
        )
        self.qtgui_number_sink_0_0.set_update_time(0.1)
        self.qtgui_number_sink_0_0.set_title("Transmit RMS")

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        units = ['', '', '', '', '',
                 '', '', '', '', '']
        colors = [("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"),
                  ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black"), ("black", "black")]
        factor = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        for i in xrange(1):
            self.qtgui_number_sink_0_0.set_min(i, -80)
            self.qtgui_number_sink_0_0.set_max(i, 10)
            self.qtgui_number_sink_0_0.set_color(i, colors[i][0], colors[i][1])
            if len(labels[i]) == 0:
                self.qtgui_number_sink_0_0.set_label(i, "Data {0}".format(i))
            else:
                self.qtgui_number_sink_0_0.set_label(i, labels[i])
            self.qtgui_number_sink_0_0.set_unit(i, units[i])
            self.qtgui_number_sink_0_0.set_factor(i, factor[i])

        self.qtgui_number_sink_0_0.enable_autoscale(False)
        self._qtgui_number_sink_0_0_win = sip.wrapinstance(self.qtgui_number_sink_0_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_number_sink_0_0_win)
        self.qtgui_freq_sink_x_1 = qtgui.freq_sink_c(
        	1024, #size
        	firdes.WIN_BLACKMAN_hARRIS, #wintype
        	0, #fc
        	samp_rate, #bw
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_freq_sink_x_1.set_update_time(0.10)
        self.qtgui_freq_sink_x_1.set_y_axis(-140, 10)
        self.qtgui_freq_sink_x_1.set_y_label('Relative Gain', 'dB')
        self.qtgui_freq_sink_x_1.set_trigger_mode(qtgui.TRIG_MODE_FREE, 0.0, 0, "")
        self.qtgui_freq_sink_x_1.enable_autoscale(False)
        self.qtgui_freq_sink_x_1.enable_grid(False)
        self.qtgui_freq_sink_x_1.set_fft_average(0.2)
        self.qtgui_freq_sink_x_1.enable_axis_labels(True)
        self.qtgui_freq_sink_x_1.enable_control_panel(False)

        if not True:
          self.qtgui_freq_sink_x_1.disable_legend()

        if "complex" == "float" or "complex" == "msg_float":
          self.qtgui_freq_sink_x_1.set_plot_pos_half(not True)

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "green", "black", "cyan",
                  "magenta", "yellow", "dark red", "dark green", "dark blue"]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_freq_sink_x_1.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_freq_sink_x_1.set_line_label(i, labels[i])
            self.qtgui_freq_sink_x_1.set_line_width(i, widths[i])
            self.qtgui_freq_sink_x_1.set_line_color(i, colors[i])
            self.qtgui_freq_sink_x_1.set_line_alpha(i, alphas[i])

        self._qtgui_freq_sink_x_1_win = sip.wrapinstance(self.qtgui_freq_sink_x_1.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_freq_sink_x_1_win)
        self.qtgui_const_sink_x_0 = qtgui.const_sink_c(
        	1024, #size
        	"", #name
        	1 #number of inputs
        )
        self.qtgui_const_sink_x_0.set_update_time(0.10)
        self.qtgui_const_sink_x_0.set_y_axis(-2, 2)
        self.qtgui_const_sink_x_0.set_x_axis(-2, 2)
        self.qtgui_const_sink_x_0.set_trigger_mode(qtgui.TRIG_MODE_FREE, qtgui.TRIG_SLOPE_POS, 0.0, 0, "")
        self.qtgui_const_sink_x_0.enable_autoscale(False)
        self.qtgui_const_sink_x_0.enable_grid(False)
        self.qtgui_const_sink_x_0.enable_axis_labels(True)

        if not True:
          self.qtgui_const_sink_x_0.disable_legend()

        labels = ['', '', '', '', '',
                  '', '', '', '', '']
        widths = [1, 1, 1, 1, 1,
                  1, 1, 1, 1, 1]
        colors = ["blue", "red", "red", "red", "red",
                  "red", "red", "red", "red", "red"]
        styles = [0, 0, 0, 0, 0,
                  0, 0, 0, 0, 0]
        markers = [0, 0, 0, 0, 0,
                   0, 0, 0, 0, 0]
        alphas = [1.0, 1.0, 1.0, 1.0, 1.0,
                  1.0, 1.0, 1.0, 1.0, 1.0]
        for i in xrange(1):
            if len(labels[i]) == 0:
                self.qtgui_const_sink_x_0.set_line_label(i, "Data {0}".format(i))
            else:
                self.qtgui_const_sink_x_0.set_line_label(i, labels[i])
            self.qtgui_const_sink_x_0.set_line_width(i, widths[i])
            self.qtgui_const_sink_x_0.set_line_color(i, colors[i])
            self.qtgui_const_sink_x_0.set_line_style(i, styles[i])
            self.qtgui_const_sink_x_0.set_line_marker(i, markers[i])
            self.qtgui_const_sink_x_0.set_line_alpha(i, alphas[i])

        self._qtgui_const_sink_x_0_win = sip.wrapinstance(self.qtgui_const_sink_x_0.pyqwidget(), Qt.QWidget)
        self.top_grid_layout.addWidget(self._qtgui_const_sink_x_0_win)
        self.marmote3_test_message_source_0 = marmote3.test_message_source(max_data_len-6-4, max_data_len-6-4, 0, 200000, 200, 0, 101, 102)
        self.marmote3_test_message_source_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_subcarrier_remapper_0 = marmote3.subcarrier_remapper(num_channels, (subcarrier_inv), 0)
        self.marmote3_subcarrier_allocator_0 = marmote3.subcarrier_allocator(num_channels, (active_channels), (), 3, getattr(self, '') if '' else marmote3.vector_peak_probe_sptr(), (), getattr(self, '') if '' else marmote3.modem_sender2_sptr(), False)
        self.marmote3_subcarrier_allocator_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_polyphase_synt_filter_ccf_0 = marmote3.polyphase_synt_filter_ccf(fbmc_fft_len, (fbmc_tx_taps), fbmc_tx_stride or fbmc_fft_len)
        self.marmote3_polyphase_synt_filter_ccf_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_polyphase_rotator_vcc_0 = marmote3.polyphase_rotator_vcc(fbmc_fft_len, (subcarrier_map), fbmc_tx_stride)
        self.marmote3_periodic_multiply_const_cc_1 = marmote3.periodic_multiply_const_cc(1, (np.exp(1.0j * np.pi / fbmc_fft_len * np.arange(0, 2 * fbmc_fft_len))))
        self.marmote3_peak_probe_0 = marmote3.peak_probe()
        self.marmote3_peak_probe_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_packet_symb_mod_0 = marmote3.packet_symb_mod(marmote3.MOD_MCS or 0, max_symb_len)
        self.marmote3_packet_ra_encoder_0 = marmote3.packet_ra_encoder(-1 if True else max_data_len, max_code_len)
        self.marmote3_packet_preamb_insert_0 = marmote3.packet_preamb_insert((np.concatenate([dc_chirp[-padding_len:],dc_chirp,dc_chirp[:padding_len]])), False, 0, marmote3.HDR_NONE, False, max_frame_len)
        self.marmote3_modem_sender2_0 = marmote3.modem_sender2(max_data_len, max_code_len, max_symb_len, 200000, 10, -1, -1, False, 0, -1, (), 0, False, False)
        self.marmote3_modem_sender2_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.marmote3_header_compressor_0 = marmote3.header_compressor(0, True)
        self.marmote3_header_compressor_0.register_monitor(getattr(self, 'modem_monitor', marmote3.modem_monitor_sptr()))
        self.fft_vxx_0 = fft.fft_vcc(fbmc_fft_len, False, (), False, 1)
        self.blocks_throttle_0 = blocks.throttle(gr.sizeof_gr_complex*1, samp_rate,True)
        self.blocks_rms_xx_0 = blocks.rms_cf(0.0001)
        self.blocks_nlog10_ff_0 = blocks.nlog10_ff(20, 1, 0)



        ##################################################
        # Connections
        ##################################################
        self.msg_connect((self.marmote3_header_compressor_0, 'out'), (self.marmote3_modem_sender2_0, 'in'))
        self.msg_connect((self.marmote3_test_message_source_0, 'out'), (self.marmote3_header_compressor_0, 'in'))
        self.connect((self.blocks_nlog10_ff_0, 0), (self.qtgui_number_sink_0_0, 0))
        self.connect((self.blocks_rms_xx_0, 0), (self.blocks_nlog10_ff_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.blocks_rms_xx_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.marmote3_peak_probe_0, 0))
        self.connect((self.blocks_throttle_0, 0), (self.qtgui_freq_sink_x_1, 0))
        self.connect((self.fft_vxx_0, 0), (self.marmote3_polyphase_synt_filter_ccf_0, 0))
        self.connect((self.marmote3_modem_sender2_0, 0), (self.marmote3_packet_ra_encoder_0, 0))
        self.connect((self.marmote3_packet_preamb_insert_0, 0), (self.marmote3_subcarrier_allocator_0, 0))
        self.connect((self.marmote3_packet_ra_encoder_0, 0), (self.marmote3_packet_symb_mod_0, 0))
        self.connect((self.marmote3_packet_symb_mod_0, 0), (self.marmote3_packet_preamb_insert_0, 0))
        self.connect((self.marmote3_packet_symb_mod_0, 0), (self.qtgui_const_sink_x_0, 0))
        self.connect((self.marmote3_periodic_multiply_const_cc_1, 0), (self.blocks_throttle_0, 0))
        self.connect((self.marmote3_polyphase_rotator_vcc_0, 0), (self.marmote3_subcarrier_remapper_0, 0))
        self.connect((self.marmote3_polyphase_synt_filter_ccf_0, 0), (self.marmote3_periodic_multiply_const_cc_1, 0))
        self.connect((self.marmote3_subcarrier_allocator_0, 0), (self.marmote3_polyphase_rotator_vcc_0, 0))
        self.connect((self.marmote3_subcarrier_remapper_0, 0), (self.fft_vxx_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "test_modem_papr")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_num_channels(self):
        return self.num_channels

    def set_num_channels(self, num_channels):
        self.num_channels = num_channels
        self.set_subcarrier_map(range(0, self.num_channels/2) + range(self.fbmc_fft_len-self.num_channels/2, self.fbmc_fft_len))
        self.set_fbmc_tx_stride({16: 22, 20: 28, 24: 34, 32: 46, 40: 58}[self.num_channels])
        self.set_fbmc_fft_len(int(self.num_channels * 5 / 4))

    def get_max_data_len(self):
        return self.max_data_len

    def set_max_data_len(self, max_data_len):
        self.max_data_len = max_data_len
        self.set_max_symb_len(marmote3.get_mcscheme(0).get_symb_len(self.max_data_len))
        self.set_max_code_len(marmote3.get_mcscheme(0).get_code_len(self.max_data_len))

    def get_fbmc_fft_len(self):
        return self.fbmc_fft_len

    def set_fbmc_fft_len(self, fbmc_fft_len):
        self.fbmc_fft_len = fbmc_fft_len
        self.set_subcarrier_map(range(0, self.num_channels/2) + range(self.fbmc_fft_len-self.num_channels/2, self.fbmc_fft_len))
        self.set_subcarrier_inv([subcarrier_map.index(x) if x in self.subcarrier_map else -1 for x in range(self.fbmc_fft_len)])
        self.set_fbmc_tx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "tx"))
        self.marmote3_periodic_multiply_const_cc_1.set_vals((np.exp(1.0j * np.pi / self.fbmc_fft_len * np.arange(0, 2 * self.fbmc_fft_len))))

    def get_chirp_len(self):
        return self.chirp_len

    def set_chirp_len(self, chirp_len):
        self.chirp_len = chirp_len
        self.set_max_frame_len(40 + self.padding_len + self.chirp_len + self.padding_len + self.max_symb_len)
        self.set_dc_chirp(self.chirp + np.ones(self.chirp_len) * 0.5)
        self.set_chirp(marmote3.get_chirp_taps(self.chirp_len))

    def get_subcarrier_map(self):
        return self.subcarrier_map

    def set_subcarrier_map(self, subcarrier_map):
        self.subcarrier_map = subcarrier_map
        self.set_subcarrier_inv([subcarrier_map.index(x) if x in self.subcarrier_map else -1 for x in range(self.fbmc_fft_len)])

    def get_padding_len(self):
        return self.padding_len

    def set_padding_len(self, padding_len):
        self.padding_len = padding_len
        self.set_max_frame_len(40 + self.padding_len + self.chirp_len + self.padding_len + self.max_symb_len)

    def get_max_symb_len(self):
        return self.max_symb_len

    def set_max_symb_len(self, max_symb_len):
        self.max_symb_len = max_symb_len
        self.set_max_frame_len(40 + self.padding_len + self.chirp_len + self.padding_len + self.max_symb_len)

    def get_fbmc_tx_stride(self):
        return self.fbmc_tx_stride

    def set_fbmc_tx_stride(self, fbmc_tx_stride):
        self.fbmc_tx_stride = fbmc_tx_stride
        self.set_fbmc_tx_taps(marmote3.get_fbmc_taps8(self.fbmc_fft_len, self.fbmc_tx_stride, "tx"))

    def get_chirp(self):
        return self.chirp

    def set_chirp(self, chirp):
        self.chirp = chirp
        self.set_dc_chirp(self.chirp + np.ones(self.chirp_len) * 0.5)

    def get_variable_marmote3_modem_monitor_0(self):
        return self.variable_marmote3_modem_monitor_0

    def set_variable_marmote3_modem_monitor_0(self, variable_marmote3_modem_monitor_0):
        self.variable_marmote3_modem_monitor_0 = variable_marmote3_modem_monitor_0

    def get_subcarrier_inv(self):
        return self.subcarrier_inv

    def set_subcarrier_inv(self, subcarrier_inv):
        self.subcarrier_inv = subcarrier_inv

    def get_samp_rate(self):
        return self.samp_rate

    def set_samp_rate(self, samp_rate):
        self.samp_rate = samp_rate
        self.qtgui_freq_sink_x_1.set_frequency_range(0, self.samp_rate)
        self.blocks_throttle_0.set_sample_rate(self.samp_rate)

    def get_max_frame_len(self):
        return self.max_frame_len

    def set_max_frame_len(self, max_frame_len):
        self.max_frame_len = max_frame_len

    def get_max_code_len(self):
        return self.max_code_len

    def set_max_code_len(self, max_code_len):
        self.max_code_len = max_code_len

    def get_fbmc_tx_taps(self):
        return self.fbmc_tx_taps

    def set_fbmc_tx_taps(self, fbmc_tx_taps):
        self.fbmc_tx_taps = fbmc_tx_taps

    def get_dc_chirp(self):
        return self.dc_chirp

    def set_dc_chirp(self, dc_chirp):
        self.dc_chirp = dc_chirp

    def get_chirp_fft_len(self):
        return self.chirp_fft_len

    def set_chirp_fft_len(self, chirp_fft_len):
        self.chirp_fft_len = chirp_fft_len

    def get_active_channels(self):
        return self.active_channels

    def set_active_channels(self, active_channels):
        self.active_channels = active_channels
        self.marmote3_subcarrier_allocator_0.set_active_channels((self.active_channels))


def main(top_block_cls=test_modem_papr, options=None):

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls()
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
