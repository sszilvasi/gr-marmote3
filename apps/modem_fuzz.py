#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from __future__ import print_function
import argparse
import pmt
import sys
import zmq
import numpy as np
from google.protobuf import json_format
import json
from marmote3 import marmote3_pb2
import random
import time


class ModemFuzzCli():

    def __init__(self):

        set_cmds = {
            "set_center_freq": ["Hz", "float"],
            "set_rf_bandwidth": ["Hz", "float"],
            "set_rx_gain": ["dB", "float"],
            "set_tx_gain": ["dB", "float"],
            "set_tx_power": ["dB", "float"],
            "set_active_channels": ["chan", "int*"],
            "set_invert_spectrum": ["flag", "int"],
            "set_enable_recording": ["flag", "int"]
        }

        center_freqs = ["997000000", "998000000", "999000000",
                        "1000000000", "1000000001", "1000000002"]
        rf_bandwidths = ["5000000", "8000000", "10000000",
                         "20000000", "25000000", "40000000"]

        try:
            while True:
                cmd = random.randint(0, 2)
                arg = random.randint(0, 5)
                if cmd == 0:
                    self.zmq_set_parameters(
                        [center_freqs[arg]], "set_center_freq", set_cmds["set_center_freq"])
                elif cmd == 1:
                    self.zmq_set_parameters(
                        [rf_bandwidths[arg]], "set_rf_bandwidth", set_cmds["set_rf_bandwidth"])
                elif cmd == 2:
                    self.zmq_set_parameters([str(arg % 16), str(
                        (arg + 2) % 16)], "set_active_channels", set_cmds["set_active_channels"])

                time.sleep(0.001)

        except KeyboardInterrupt:
            print('Exiting...')

    def zmq_set_parameters(self, args, cmd, params):
        parser = argparse.ArgumentParser(prog="modem_cli.py " + cmd,
                                         formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        parser.add_argument(
            "--addr", default="tcp://127.0.0.1:7555", help="ZMQ socket address")
        for i in range(0, len(params), 2):
            n, t = params[i], params[i + 1]
            if t == "int":
                parser.add_argument(n, type=int)
            elif t == "float":
                parser.add_argument(n, type=float)
            elif t == "int*":
                parser.add_argument(n, nargs="*", type=int)
            elif t == "float*":
                parser.add_argument(n, nargs="*", type=float)
            else:
                raise ValueError("invalid type: {}".format(t))
        opts = parser.parse_args(args)

        context = zmq.Context()
        socket = context.socket(zmq.PUSH)
        socket.connect(opts.addr)

        msg = marmote3_pb2.ModemCommand()
        if cmd == "set_center_freq":
            msg.modem_control.center_freq.value = opts.Hz
        elif cmd == "set_rf_bandwidth":
            msg.modem_control.rf_bandwidth.value = opts.Hz
        elif cmd == "set_rx_gain":
            msg.modem_control.rx_gain.value = opts.dB
        elif cmd == "set_tx_gain":
            msg.modem_control.tx_gain.value = opts.dB
        elif cmd == "set_tx_power":
            msg.modem_control.tx_power.value = opts.dB
        elif cmd == "set_invert_spectrum":
            msg.modem_control.invert_spectrum.value = opts.flag != 0
        elif cmd == "set_enable_recording":
            msg.modem_control.enable_recording.value = opts.flag != 0
        elif cmd == "set_active_channels":
            msg.subcarrier_allocator.active_channels[:] = opts.chan

        socket.send(msg.SerializeToString())

        text = json_format.MessageToJson(msg, preserving_proto_field_name=True)
        print("ZMQ message sent to {}".format(opts.addr))
        print(json.dumps(json.loads(text), indent=2, sort_keys=True))

        socket.close()


if __name__ == "__main__":
    ModemFuzzCli()
