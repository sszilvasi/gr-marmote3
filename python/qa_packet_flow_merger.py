#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import time


class qa_packet_flow_merger (gr_unittest.TestCase):

    def gen_test(self, num1, num2):
        top = gr.top_block()
        merger = marmote3.packet_flow_merger(gr.sizeof_int, 2, 100)
        sink = blocks.vector_sink_i()
        top.connect(
            blocks.vector_source_i(range(num1 * 100), False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_int, 1, 100, "packet_len"),
            (merger, 0)
        )
        top.connect(
            blocks.vector_source_i(range(num2 * 100), False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_int, 1, 100, "packet_len"),
            (merger, 1)
        )
        top.connect(merger, sink)
        top.run()
        print num1 * 100, num2 * 100, len(sink.data())
        self.assertTrue(len(sink.data()) == (num1 + num2) * 100)

    def test0(self):
        self.gen_test(0, 0)

    def test1(self):
        self.gen_test(1, 0)

    def test2(self):
        self.gen_test(0, 1)

    def test3(self):
        self.gen_test(100, 0)

    def test4(self):
        self.gen_test(0, 100)

    def test5(self):
        self.gen_test(10, 100)

    def test6(self):
        self.gen_test(100, 10)

    def test7(self):
        self.gen_test(100, 100)

    def test8(self):
        top = gr.top_block()
        splitter = marmote3.packet_flow_splitter(gr.sizeof_int, 2, 100)
        merger = marmote3.packet_flow_merger(gr.sizeof_int, 2, 100)
        sink = blocks.vector_sink_i()
        top.connect(blocks.vector_source_i(range(100000), False),
                    blocks.stream_to_tagged_stream(
            gr.sizeof_int, 1, 100, "packet_len"),
            splitter)
        top.connect((splitter, 0), (merger, 0))
        top.connect((splitter, 1), (merger, 1))
        top.connect(merger, sink)
        top.run()
        print len(sink.data())
        self.assertTrue(len(sink.data()) == 100000)

    def test9(self):
        top = gr.top_block()
        merger = marmote3.packet_flow_merger(gr.sizeof_int, 2, 5001)
        copy = blocks.copy(gr.sizeof_int)
        splitter = marmote3.packet_flow_splitter(gr.sizeof_int, 2, 5000)
        sink1 = blocks.vector_sink_i()
        sink2 = blocks.vector_sink_i()
        top.connect(
            blocks.vector_source_i(range(0, 50000), False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_int, 1, 5000, "packet_len"),
            (merger, 0)
        )
        top.connect(
            blocks.vector_source_i(range(50000, 100000), False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_int, 1, 5000, "packet_len"),
            (merger, 1)
        )
        top.connect(merger, copy, splitter)
        top.connect((splitter, 0), sink1)
        top.connect((splitter, 1), sink2)
        top.run()
        print len(sink1.data()), len(sink2.data()), len(sink1.data()) + len(sink2.data())
        print merger.nitems_written(0), copy.nitems_read(0), copy.nitems_written(0), splitter.nitems_read(0)
        a = max(max(sink1.data()), max(sink2.data()))
        b = min(min(sink1.data()), min(sink2.data()))
        c = len(sink1.data()) + len(sink2.data())
        print [x / 1000 for x in sink1.data() if x % 1000 == 1]
        print [x / 1000 for x in sink2.data() if x % 1000 == 1]
        self.assertTrue(len(sink1.data()) + len(sink2.data()) == 100000)

    def test10(self):
        top = gr.top_block()
        merger = marmote3.packet_flow_merger(gr.sizeof_int, 2, 5000)
        copy = blocks.copy(gr.sizeof_int)
        sink = blocks.vector_sink_i()
        top.connect(
            blocks.vector_source_i(range(0, 50000), False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_int, 1, 5000, "packet_len"),
            # blocks.throttle(gr.sizeof_int, 1e6),
            (merger, 0)
        )
        top.connect(
            blocks.vector_source_i(range(50000, 100000), False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_int, 1, 5000, "packet_len"),
            # blocks.throttle(gr.sizeof_int, 1e10),
            (merger, 1)
        )
        top.connect(merger, copy, sink)
        top.run()
        print len(sink.data()), merger.nitems_written(0), copy.nitems_read(0)
        print [x / 5000 for x in sink.data() if x % 5000 == 1]
        self.assertTrue(len(sink.data()) == 100000)

    def xtest11(self):
        top = gr.top_block()
        merger = marmote3.packet_flow_merger(gr.sizeof_int, 2, 500)
        sink = blocks.vector_sink_i()
        top.connect(
            blocks.vector_source_i(range(0, 500), True),
            blocks.throttle(gr.sizeof_int, 1e6),
            blocks.skiphead(gr.sizeof_int, 100000),
            blocks.head(gr.sizeof_int, 500),
            blocks.stream_to_tagged_stream(
                gr.sizeof_int, 1, 500, "packet_len"),
            (merger, 0)
        )
        top.connect(
            blocks.vector_source_i([], False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_int, 1, 500, "packet_len"),
            (merger, 1)
        )
        top.connect(merger, sink)
        top.run()
        print len(sink.data())
        self.assertTrue(list(sink.data()) == range(500))


if __name__ == '__main__':
    gr_unittest.run(qa_packet_flow_merger, "qa_packet_flow_merger.xml")
