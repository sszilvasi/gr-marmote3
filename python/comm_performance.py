import numpy as np
import math


def norm_sf(x):
    """This is an implementation of scipy.stats.norm.sf"""
    return 0.5 * math.erfc(x * math.sqrt(0.5))


def calc_mpsk_awgn_ber_vs_ebn0(M, ebn0):
    """
    Calculate the APPROXIMATE bit error probability vs. Eb/N0 (in linear units)
    for M-PSK over the AWGN channel
    """
    assert M == 2 or M == 4 or M == 8
    mm = np.log2(M)
    # Simon (8.32), approximate but good
    upp_ind = max(int(M / 4.0), 1)
    ber = np.empty_like(ebn0)
    for eb in range(len(ebn0)):
        ber[eb] = 0.0
        for ii in range(1, upp_ind + 1):
            ber[eb] += norm_sf(
                math.sqrt(2.0 * ebn0[eb] * mm) * math.sin((2 * ii - 1) * np.pi / M))
    ber *= 2.0 / max(mm, 2.0)
    return ber


def calc_qam_awgn_ber_vs_ebn0(M, ebn0):
    """
    Calculate the symbol error probability vs. Eb/N0 (in linear units)
    for square QAM over the AWGN channel
    """
    m = math.sqrt(M)
    Pb = np.zeros((ebn0.shape))
    for eb in range(len(ebn0)):
        for k in range(1, int(np.log2(m) + 1)):
            Pbk = 0.0
            for ii in range(int((1 - 2**(-k)) * m)):
                exp1 = int(2**(k - 1) * ii / float(m))
                f2 = int(2**(k - 1) * ii / float(m) + 0.5)
                bf = (-1)**exp1 * (2**(k - 1) - f2) * norm_sf(
                    (2 * ii + 1) * math.sqrt(3 * ebn0[eb] * np.log2(M) / float(M - 1)))
                Pbk += bf
            Pbk *= 2.0 / m
            Pb[eb] += Pbk
    return 1.0 / np.log2(m) * Pb
