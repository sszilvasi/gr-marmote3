#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import numpy as np
import pmt
import math
import marmote3_pb2


class qa_vector_peak_probe (gr_unittest.TestCase):

    def gen_test(self, vlen):
        top = gr.top_block()
        data = map(float, np.random.random_integers(-1000, 1000, 10 * vlen))
        probe = marmote3.vector_peak_probe(vlen, 0, 0, 0.0, 0.0)
        top.connect(blocks.vector_source_f(data, False),
                    blocks.stream_to_vector(gr.sizeof_float, 2 * vlen),
                    probe)
        top.run()

        x = []
        for i in range(vlen):
            a = np.array(data[2 * i::2 * vlen])
            b = np.array(data[2 * i + 1::2 * vlen])
            x.append(10.0 * math.log10(np.amax(a * a + b * b)))

        y = probe.get_maximum_db()
        self.assertTrue(np.allclose(x, y))

    def test1(self):
        self.gen_test(4)

    def test2(self):
        self.gen_test(16)

    def test3(self):
        self.gen_test(20)

    def test4(self):
        self.gen_test(25)


if __name__ == '__main__':
    gr_unittest.run(qa_vector_peak_probe, "qa_vector_peak_probe.xml")
