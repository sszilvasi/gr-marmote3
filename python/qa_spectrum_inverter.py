#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import numpy as np


class qa_spectrum_inverter (gr_unittest.TestCase):

    def test1(self):
        top = gr.top_block()
        data = np.random.rand(1024) + 1j * np.random.rand(1024)
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c(data, False),
            marmote3.spectrum_inverter(True),
            sink
        )
        top.run()
        self.assertTrue(np.allclose(sink.data(), np.conj(data)))

    def test2(self):
        top = gr.top_block()
        data = np.random.rand(1024) + 1j * np.random.rand(1024)
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c(data, False),
            marmote3.spectrum_inverter(False),
            sink
        )
        top.run()
        self.assertTrue(np.allclose(sink.data(), data))


if __name__ == '__main__':
    gr_unittest.run(qa_spectrum_inverter, "qa_spectrum_inverter.xml")
