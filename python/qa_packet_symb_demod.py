#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2013-2018 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

import random
from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3


class qa_packet_symb_demod (gr_unittest.TestCase):

    def gen_test(self, mod):
        data = [random.randint(0, 1) for x in range(3 * 16)]
        top = gr.top_block()
        sink = blocks.vector_sink_b()
        top.connect(
            blocks.vector_source_b(data, False),
            blocks.unpacked_to_packed_bb(1, gr.GR_LSB_FIRST),
            blocks.stream_to_tagged_stream(
                gr.sizeof_char, 1, len(data) / 8, "packet_len"),
            marmote3.packet_symb_mod(mod, len(data)),
            marmote3.packet_symb_demod(mod, 100.0, len(data)),
            sink
        )
        top.run()
        print data
        print sink.data()
        data2 = [0 if x <= 127 else 1 for x in sink.data()]
        self.assertTrue(data2 == data)

    def test1(self):
        self.gen_test(marmote3.MOD_BPSK)

    def test2(self):
        self.gen_test(marmote3.MOD_QPSK)

    def test3(self):
        self.gen_test(marmote3.MOD_8PSK)

    def test4(self):
        self.gen_test(marmote3.MOD_16QAM)

    def test5(self):
        self.gen_test(marmote3.MOD_64QAM)

if __name__ == '__main__':
    gr_unittest.run(qa_packet_symb_demod, "qa_packet_symb_demod.xml")
