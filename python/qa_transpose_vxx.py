#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import numpy as np


class qa_transpose_vxx (gr_unittest.TestCase):

    def gen_test1(self, rows, cols):
        top = gr.top_block()
        sink = blocks.vector_sink_f()
        top.connect(
            blocks.vector_source_f(
                range(3 * rows * cols - 1), False),
            blocks.stream_to_vector(gr.sizeof_float, rows),
            marmote3.transpose_vxx(gr.sizeof_float, rows, cols),
            blocks.vector_to_stream(gr.sizeof_float, cols),
            sink
        )
        top.run()
        print sink.data()
        expected = np.reshape(range(2 * cols * rows), (2, cols, rows))
        expected = np.transpose(expected, (0, 2, 1)).flatten()
        self.assertTrue(np.allclose(sink.data(), expected))

    def test1(self):
        self.gen_test1(2, 3)
        self.gen_test1(4, 4)
        self.gen_test1(2, 4)
        self.gen_test1(4, 2)
        self.gen_test1(11, 8)
        self.gen_test1(12, 9)
        self.gen_test1(7, 12)
        self.gen_test1(13, 29)

    def gen_test2(self, rows, cols):
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_c(
                range(3 * rows * cols - 1), False),
            blocks.stream_to_vector(gr.sizeof_gr_complex, rows),
            marmote3.transpose_vxx(gr.sizeof_gr_complex, rows, cols),
            blocks.vector_to_stream(gr.sizeof_gr_complex, cols),
            sink
        )
        top.run()
        print sink.data()
        expected = np.reshape(range(2 * cols * rows), (2, cols, rows))
        expected = np.transpose(expected, (0, 2, 1)).flatten()
        self.assertTrue(np.allclose(sink.data(), expected))

    def test2(self):
        self.gen_test2(2, 3)
        self.gen_test2(4, 4)
        self.gen_test2(2, 4)
        self.gen_test2(4, 2)
        self.gen_test2(11, 8)
        self.gen_test2(12, 9)
        self.gen_test2(7, 12)
        self.gen_test2(13, 29)


if __name__ == '__main__':
    gr_unittest.run(qa_transpose_vxx, "qa_transpose_vxx.xml")
