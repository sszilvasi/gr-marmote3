#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2013, 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr
import marmote3
import threading
import time


class hardware_agc(gr.hier_block2):

    def __init__(self, target, gain_var, min_gain, max_gain, min_ampl, max_ampl, rate):
        gr.hier_block2.__init__(
            self, "Receive AGC",
            gr.io_signature(1, 1, gr.sizeof_gr_complex),
            gr.io_signature(0, 0, 0))

        self.gain_var = gain_var
        self.get_gain = getattr(target, "get_" + gain_var, lambda: min_gain)
        self.set_gain = getattr(target, "set_" + gain_var, lambda gain: None)
        self.min_gain = min_gain
        self.max_gain = max_gain
        self.min_ampl = min_ampl
        self.max_ampl = max_ampl
        self.rate = 1.0 / max(rate, 0.1)
        self.init_gain = None

        self.peak_probe = marmote3.peak_probe()
        self.connect(self, self.peak_probe)

        self.worker = threading.Thread(target=self.worker)
        self.worker.daemon = True
        self.worker.start()

    def worker(self):
        try:
            while True:
                time.sleep(self.rate)
                peak = self.peak_probe.get_peak()
                gain = self.get_gain()
                if peak > self.max_ampl and gain > self.min_gain:
                    step = 3 if peak > 2 * self.max_ampl else 1
                    gain = max(self.min_gain, gain - step)
                    self.set_gain(gain)
                    print("hardware_agc: changed {} to {} peak {}".format(
                        self.gain_var, gain, peak))
                elif peak < self.min_ampl and gain < self.max_gain:
                    gain = min(self.max_gain, gain + 1)
                    self.set_gain(gain)
                    print("hardware_agc: changed {} to {} peak {}".format(
                        self.gain_var, gain, peak))
                else:
                    print("hardware_agc: current {} at {} peak {}".format(
                        self.gain_var, gain, peak))

        except Exception as err:
            print("##### hardware_agc: crashed", err)
