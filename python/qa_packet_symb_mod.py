#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2013-2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

import random
from gnuradio import gr, gr_unittest
from gnuradio import blocks
import marmote3_swig as marmote3
import numpy as np


class qa_packet_symb_mod (gr_unittest.TestCase):

    def gen_test(self, mod):
        data = [random.randint(0, 255) for x in range(600)]
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_b(data, False),
            blocks.stream_to_tagged_stream(
                gr.sizeof_char, 1, len(data), "packet_len"),
            marmote3.packet_symb_mod(mod, len(data)),
            sink
        )
        top.run()
        energy = np.absolute(sink.data())
        print energy
        self.assertEqual(len(energy), 600 / marmote3.get_modulation_order(mod))
        self.assertTrue(np.allclose(energy, np.ones(len(energy))))

    def xtest1(self):
        self.gen_test(marmote3.MOD_BPSK)

    def xtest2(self):
        self.gen_test(marmote3.MOD_QPSK)

    def xtest3(self):
        self.gen_test(marmote3.MOD_8PSK)

    def test4(self):
        data = []
        for i in range(16):
            for j in range(4):
                data.append((i >> j) & 1)
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_b(data, False),
            blocks.unpacked_to_packed_bb(1, gr.GR_LSB_FIRST),
            blocks.stream_to_tagged_stream(gr.sizeof_char, 1, 8, "packet_len"),
            marmote3.packet_symb_mod(marmote3.MOD_16QAM, 16),
            sink
        )
        top.run()
        energy2 = np.square(np.absolute(sink.data()))
        print energy2
        print np.mean(energy2)
        self.assertTrue(np.allclose(np.mean(energy2), 1))
        self.assertTrue(
            np.allclose(energy2, [1.8, 1, 1.8, 1, 1, 0.2, 1, 0.2, 1.8, 1, 1.8, 1, 1, 0.2, 1, 0.2]))

    def test5(self):
        data = []
        for i in range(3 * 64):
            for j in range(6):
                data.append((i >> j) & 1)
        top = gr.top_block()
        sink = blocks.vector_sink_c()
        top.connect(
            blocks.vector_source_b(data, False),
            blocks.unpacked_to_packed_bb(1, gr.GR_LSB_FIRST),
            blocks.stream_to_tagged_stream(
                gr.sizeof_char, 1, 12, "packet_len"),
            marmote3.packet_symb_mod(marmote3.MOD_64QAM, 16),
            sink
        )
        top.run()
        energy2 = np.square(np.absolute(sink.data()))
        print energy2, np.mean(energy2)
        self.assertTrue(np.allclose(np.mean(energy2), 1))

if __name__ == '__main__':
    gr_unittest.run(qa_packet_symb_mod, "qa_packet_symb_mod.xml")
