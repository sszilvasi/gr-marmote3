#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

"""
This module tests the performance of the repeat accumulate coder with
a small flowgraph.
"""

import marmote3
import numpy as np
import matplotlib.pyplot as plt

from test_ber_ra_coder import test_ber_ra_coder


NUM_PACKETS = 2000


def ebn0_to_noise_power(rate, ebn0):
    """Assuming QPSK modulation and 1.0 signal power, calculates
    the power of the noise for the given EbN0 and coding rate"""
    assert 0 <= rate <= 1
    return 0.5 / ((10 ** (0.1 * ebn0)) * rate)


def plot_ber_ra_coder(ebn0s, data_len, code_len):
    bers = np.zeros([len(ebn0s)])

    for idx, ebn0 in enumerate(ebn0s):
        g = test_ber_ra_coder(
            data_len=data_len,
            code_len=code_len,
            num_packets=NUM_PACKETS,
            full_passes=1,
            half_passes=15,
            noise_power=ebn0_to_noise_power(
                float(data_len) / code_len, ebn0)
        )
        g.run()
        bers[idx] = g.marmote3_modem_bert_receiver_0.get_ber()

    plt.semilogy(ebn0s, bers, label="QPSK RA {}/{}".format(
        data_len, code_len), marker="x")


def ebn0_plot(data_len=1000):
    plt.figure()
    ebn0s = np.arange(0, 6.1, 0.1)

    for r in [1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.7, 2.0, 3.0, 4.0]:
        plot_ber_ra_coder(ebn0s, data_len, int(data_len * r))

    bers = marmote3.calc_mpsk_awgn_ber_vs_ebn0(4, 10**(ebn0s / 10.0))
    plt.semilogy(ebn0s, bers, label="QPSK uncoded", marker="s")

    plt.xlabel('Eb/N0')
    plt.ylabel('BER')
    plt.legend()
    plt.grid()
    plt.show()


def data_len_plot1():
    plt.figure()
    ebn0s = np.arange(2, 3.6, 0.2)

    for data_len in range(1000, 1004, 1):
        for code_len in range(1500, 1504, 2):
            plot_ber_ra_coder(ebn0s, data_len, code_len)

    plt.xlabel('Eb/N0')
    plt.ylabel('BER')
    plt.legend()
    plt.grid()
    plt.show()


def data_len_plot2():
    plt.figure()
    ebn0s = np.arange(2.0, 8.5, 0.5)

    for data_len in range(23, 25, 1):
        plot_ber_ra_coder(ebn0s, data_len, 80)

    plt.xlabel('Eb/N0')
    plt.ylabel('BER')
    plt.legend()
    plt.grid()
    plt.show()


if __name__ == '__main__':
    # ebn0_plot()
    data_len_plot1()
