#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Test Ber Soft Demod
# Generated: Fri Aug 17 23:17:22 2018
##################################################

from gnuradio import analog
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import gr
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from optparse import OptionParser
import marmote3
import math


class test_ber_soft_demod(gr.top_block):

    def __init__(self, data_len=1500, mod_index=2, rep=2, snr=10.0):
        gr.top_block.__init__(self, "Test Ber Soft Demod")

        ##################################################
        # Parameters
        ##################################################
        self.data_len = data_len
        self.mod_index = mod_index
        self.rep = rep
        self.snr = snr

        ##################################################
        # Blocks
        ##################################################
        self.marmote3_packet_symb_mod_0 = marmote3.packet_symb_mod(None or mod_index, data_len * 8 * rep)
        self.marmote3_packet_symb_demod_0 = marmote3.packet_symb_demod(None or mod_index, 24, data_len * 8 * rep)
        self.marmote3_packet_rep_encoder_0 = marmote3.packet_rep_encoder(data_len*8,rep)
        self.marmote3_packet_rep_decoder_0 = marmote3.packet_rep_decoder(data_len,rep)
        self.marmote3_modem_bert_sender_0 = marmote3.modem_bert_sender(1, 10000, data_len)
        self.marmote3_modem_bert_receiver_0 = marmote3.modem_bert_receiver(1)
        self.blocks_add_xx_0 = blocks.add_vcc(1)
        (self.blocks_add_xx_0).set_min_output_buffer(1048576)
        self.analog_noise_source_x_0 = analog.noise_source_c(analog.GR_GAUSSIAN, 1.0 / math.sqrt(10**(float(snr) / 10.0)), 0)



        ##################################################
        # Connections
        ##################################################
        self.connect((self.analog_noise_source_x_0, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.blocks_add_xx_0, 0), (self.marmote3_packet_symb_demod_0, 0))
        self.connect((self.marmote3_modem_bert_sender_0, 0), (self.marmote3_packet_rep_encoder_0, 0))
        self.connect((self.marmote3_packet_rep_decoder_0, 0), (self.marmote3_modem_bert_receiver_0, 0))
        self.connect((self.marmote3_packet_rep_encoder_0, 0), (self.marmote3_packet_symb_mod_0, 0))
        self.connect((self.marmote3_packet_symb_demod_0, 0), (self.marmote3_packet_rep_decoder_0, 0))
        self.connect((self.marmote3_packet_symb_mod_0, 0), (self.blocks_add_xx_0, 0))

    def get_data_len(self):
        return self.data_len

    def set_data_len(self, data_len):
        self.data_len = data_len

    def get_mod_index(self):
        return self.mod_index

    def set_mod_index(self, mod_index):
        self.mod_index = mod_index

    def get_rep(self):
        return self.rep

    def set_rep(self, rep):
        self.rep = rep

    def get_snr(self):
        return self.snr

    def set_snr(self, snr):
        self.snr = snr
        self.analog_noise_source_x_0.set_amplitude(1.0 / math.sqrt(10**(float(self.snr) / 10.0)))


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    parser.add_option(
        "-d", "--data-len", dest="data_len", type="intx", default=1500,
        help="Set data length [default=%default]")
    parser.add_option(
        "-m", "--mod-index", dest="mod_index", type="intx", default=2,
        help="Set modulation order [default=%default]")
    parser.add_option(
        "-r", "--rep", dest="rep", type="intx", default=2,
        help="Set Repetition [default=%default]")
    parser.add_option(
        "-s", "--snr", dest="snr", type="eng_float", default=eng_notation.num_to_str(10.0),
        help="Set SNR [default=%default]")
    return parser


def main(top_block_cls=test_ber_soft_demod, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    tb = top_block_cls(data_len=options.data_len, mod_index=options.mod_index, rep=options.rep, snr=options.snr)
    tb.start()
    tb.wait()


if __name__ == '__main__':
    main()
