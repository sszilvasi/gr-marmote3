#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2017 Miklos Maroti.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio.filter import firdes, optfir
import numpy as np
from scipy import signal, optimize
import matplotlib.pyplot as plt
from filters import *


def print_stats(taps):
    taps = np.array(taps)
    print "taps length:", len(taps)
    print "taps[0]:    ", taps[0]
    print "taps[mid-1]:", taps[len(taps) / 2 - 1]
    print "taps[mid]:  ", taps[len(taps) / 2]
    print "taps[mid+1]:", taps[len(taps) / 2 + 1]
    print "taps[-1]:   ", taps[-1]
    norm = get_norm(taps)
    print "max norm:", np.max(norm)
    print "avg norm:", np.mean(norm)
    print "sum norm:", np.sum(norm)


def print_cross_corrs(taps, stride=1):
    for i in range(0, len(taps), stride):
        s1 = np.pad(taps, (0, i), mode='constant')
        s2 = np.pad(taps, (i, 0), mode='constant')
        print i, np.dot(s1, s2)


def print_taps(taps):
    print "[" + ", ".join(map(str, taps)) + "]"


def calc_ici(chan_sep, taps, skip=0):
    """Calculates the inter channel interference for the given
    number of chanells, basically the amount of energy put out
    into all other channels. You can use the skip functionality
    to skip extra 1/101-th part of the neighboring channel."""
    pwr = get_norm(np.fft.fft(taps, n=101 * chan_sep))
    return np.sum(pwr[51 + skip:-50 - skip]) / np.sum(pwr)


def calc_ici2(chan_sep, tx_taps, rx_taps):
    rot = np.exp(2.0j * np.pi * np.arange(len(tx_taps)) / chan_sep)
    pwr = [np.sum(get_norm(np.convolve((rot ** i) * tx_taps, rx_taps)))
           for i in range(chan_sep)]
    return np.sum(pwr[1:]) / np.sum(pwr)


def calc_isi(symb_sep, taps):
    """Calculates the inter symbol interference for the given
    symbol per sample rate and taps."""
    assert len(taps) % 2 == 1
    dly = (len(taps) - 1) / 2
    pwr = np.square(taps[dly % symb_sep::symb_sep])
    # print pwr
    return (np.sum(pwr) - pwr[dly / symb_sep]) / np.sum(pwr)


def calc_nsr(chan_sep, tx_taps, rx_taps, nsr=0.1):
    """Assuming that the transmitted signal has the given
    noise to signal ratio, calculates the noise to signal
    ratio after applying the receive filter."""
    e1 = np.sum(get_norm(tx_taps))
    e2 = np.sum(get_norm(rx_taps))
    e3 = np.sum(get_norm(np.convolve(tx_taps, rx_taps)))
    return (e1 * chan_sep * nsr * e2) / e3


def calc_papr(symb_sep, tx_taps):
    extra = len(tx_taps) % symb_sep
    if extra > 0:
        tx_taps = np.pad(tx_taps, (0, symb_sep - extra), mode="constant")
    tx_taps = np.reshape(tx_taps, (len(tx_taps) / symb_sep, symb_sep))
    peak = np.max(np.sum(np.abs(tx_taps), axis=0))
    norm = np.sum(get_norm(tx_taps)) ** 0.5
    return peak / norm


def plot_filters(taps_list):
    plt.figure()
    for taps in taps_list:
        plt.plot(taps)
    plt.show()


def plot_spectrum(sep, taps, show=True):
    taps = taps / ((sep * np.sum(np.square(taps))) ** 0.5)
    if show:
        plt.figure()
    w, h = signal.freqz(taps, worN=100 * sep, whole=True)
    w = w[:200] / (2.0 * np.pi) * sep
    plt.plot(w, 20.0 * np.log10(np.abs(h[:200])))
    plt.plot(w, 20.0 * np.log10(np.abs(np.roll(h, 100)[:200])))
    plt.plot(w, 20.0 * np.log10(np.abs(np.roll(h, 200)[:200])))
    h = np.tile(np.sum(np.reshape(np.abs(h), [sep, 100]), axis=0), 2)
    plt.plot(w, 20.0 * np.log10(h))
    plt.gca().set_ylim([-80, 10])
    if show:
        plt.show()


def plot_ambiguity_dumb(taps, stride, df=512):
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D
    from matplotlib import cm

    maxtau = 4 * stride
    nu_vals = np.linspace(-1.0, 1.0, df)

    af = np.empty((2 * maxtau, len(nu_vals)), dtype=np.complex128)

    for tau in xrange(maxtau):
        s1 = np.pad(taps, (0, tau), mode='constant')
        s2 = np.pad(np.conjugate(taps), (tau, 0), mode='constant')
        for nu_ in xrange(len(nu_vals)):
            nu = nu_vals[nu_]
            s2m = np.multiply(s2, np.exp(-2j * nu * np.arange(len(s2))))
            af[maxtau + tau, nu_] = np.dot(s1, s2m)
            af[maxtau - tau, nu_] = np.dot(s2m, s1)

    X, Y = np.meshgrid(nu_vals, np.arange(-maxtau, maxtau))
    Z = 20.0 * np.log10(np.abs(af))
    # Z = np.abs(af)

    # fig = plt.figure()

    # ax = fig.gca(projection='3d')
    # surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
    #                   linewidth=0, antialiased=False)
    # fig.colorbar(surf, shrink=0.5, aspect=5)

    CS = plt.contour(X, Y, Z)
    plt.show()


def calc_ambiguity_dumb(taps, stride, df=512):
    maxtau = 4 * stride
    nu_vals = np.linspace(-1.0, 1.0, df)

    af = np.empty((2 * maxtau, len(nu_vals)), dtype=np.complex128)

    for tau in xrange(maxtau):
        s1 = np.pad(taps, (0, tau), mode='constant')
        s2 = np.pad(np.conjugate(taps), (tau, 0), mode='constant')
        for nu_ in xrange(len(nu_vals)):
            nu = nu_vals[nu_]
            s2m = np.multiply(s2, np.exp(-2j * nu * np.arange(len(s2))))
            af[maxtau + tau, nu_] = np.dot(s1, s2m)
            af[maxtau - tau, nu_] = np.dot(s2m, s1)

    return af


def plot_af_nu_cut(af, stride):
    import matplotlib.pyplot as plt
    no_freqs = af.shape[0]
    plt.figure()
    plt.plot(20 * np.log10(np.abs(af[no_freqs / 2, :])))
    plt.show()


def plot_af_tau_cut(af, stride):
    import matplotlib.pyplot as plt
    no_freqs = af.shape[1]
    no_taus = af.shape[0]
    plt.figure()
    plt.plot(np.arange(-no_taus / 2, no_taus / 2),
             20 * np.log10(np.abs(af[:, no_freqs / 2])))
    plt.show()


def optimize1(chan_sep, symb_sep, taps, skip=0, nsr=0.1, ici1_weight=1, ici2_weight=1):
    assert len(taps) % 2 == 1
    mid = (len(taps) + 1) / 2

    def fun(vars):
        taps2 = np.concatenate([vars, vars[-2::-1]])
        return ici1_weight * calc_ici(chan_sep, taps2, skip=skip) + \
            ici2_weight * calc_ici2(chan_sep, taps2, taps2) + \
            calc_nsr(chan_sep, taps2, taps2, nsr=nsr) + \
            calc_isi(symb_sep, np.convolve(taps2, taps2))

    res = optimize.minimize(
        fun,
        taps[:mid],
        options={"maxiter": 500,
                 "gtol": 1e-7}
    )["x"]
    taps2 = np.concatenate([res, res[-2::-1]])
    return taps2 / (np.sum(np.square(taps2)) ** 0.5)


def optimize2(chan_sep, symb_sep, tx_taps, rx_taps, skip=0, nsr=0.1, ici1_weight=1, ici2_weight=1):
    assert len(tx_taps) % 2 == 1 and len(rx_taps) % 2 == 1
    tx_mid = (len(tx_taps) + 1) / 2
    rx_mid = (len(rx_taps) + 1) / 2

    def fun(vars):
        tx_taps2 = np.concatenate([vars[:tx_mid], vars[tx_mid - 2::-1]])
        rx_taps2 = np.concatenate([vars[tx_mid:], vars[-2:tx_mid - 1:-1]])
        return ici1_weight * calc_ici(chan_sep, tx_taps2, skip=skip) + \
            ici2_weight * calc_ici2(chan_sep, tx_taps2, rx_taps2) + \
            calc_nsr(chan_sep, tx_taps2, rx_taps2, nsr=nsr) + \
            calc_isi(symb_sep, np.convolve(tx_taps2, rx_taps2))

    res = optimize.minimize(
        fun,
        np.concatenate([tx_taps[:tx_mid], rx_taps[:rx_mid]]),
        options={"maxiter": 500,
                 "gtol": 1e-7}
    )["x"]
    tx_taps2 = np.concatenate([res[:tx_mid], res[tx_mid - 2::-1]])
    rx_taps2 = np.concatenate([res[tx_mid:], res[-2:tx_mid - 1:-1]])
    return tx_taps2 / (np.sum(np.square(tx_taps2)) ** 0.5), \
        rx_taps2 / (np.sum(np.square(rx_taps2)) ** 0.5)


def optimize3(chan_sep, symb_sep, tx_taps, rx_taps, skip1=0, skip2=0, ici1_weight=1, ici2_weight=1):
    assert len(tx_taps) % 2 == 1 and len(rx_taps) % 2 == 1
    tx_mid = (len(tx_taps) + 1) / 2
    rx_mid = (len(rx_taps) + 1) / 2

    def fun(vars):
        tx_taps2 = np.concatenate([vars[:tx_mid], vars[tx_mid - 2::-1]])
        rx_taps2 = np.concatenate([vars[tx_mid:], vars[-2:tx_mid - 1:-1]])
        return ici1_weight * calc_ici(chan_sep, tx_taps2, skip=skip1) + \
            ici2_weight * calc_ici(chan_sep, rx_taps2, skip=skip2) + \
            0.1 * calc_isi(symb_sep, tx_taps2) + \
            calc_isi(symb_sep, np.convolve(tx_taps2, rx_taps2))

    res = optimize.minimize(
        fun,
        np.concatenate([tx_taps[:tx_mid], rx_taps[:rx_mid]]),
        options={"maxiter": 500,
                 "gtol": 1e-7}
    )["x"]
    tx_taps2 = np.concatenate([res[:tx_mid], res[tx_mid - 2::-1]])
    rx_taps2 = np.concatenate([res[tx_mid:], res[-2:tx_mid - 1:-1]])
    return tx_taps2 / (np.sum(np.square(tx_taps2)) ** 0.5), \
        rx_taps2 / (np.sum(np.square(rx_taps2)) ** 0.5)


def optimize4(chan_sep, symb_sep, tx_taps, rx_taps, skip1=0, ici1_weight=1, ici2_weight=1, isi1_weight=1):
    assert len(tx_taps) % 2 == 1 and len(rx_taps) % 2 == 1
    tx_mid = (len(tx_taps) + 1) / 2
    rx_mid = (len(rx_taps) + 1) / 2

    def fun(vars):
        tx_taps2 = np.concatenate([vars[:tx_mid], vars[tx_mid - 2::-1]])
        rx_taps2 = np.concatenate([vars[tx_mid:], vars[-2:tx_mid - 1:-1]])
        return ici1_weight * calc_ici(chan_sep, tx_taps2, skip=skip1) + \
            ici2_weight * calc_ici2(chan_sep, tx_taps2, rx_taps2) + \
            isi1_weight * calc_isi(symb_sep, tx_taps2) + \
            calc_isi(symb_sep, np.convolve(tx_taps2, rx_taps2))

    res = optimize.minimize(
        fun,
        np.concatenate([tx_taps[:tx_mid], rx_taps[:rx_mid]]),
        options={"maxiter": 500,
                 "gtol": 1e-7}
    )["x"]
    tx_taps2 = np.concatenate([res[:tx_mid], res[tx_mid - 2::-1]])
    rx_taps2 = np.concatenate([res[tx_mid:], res[-2:tx_mid - 1:-1]])
    return tx_taps2 / (np.sum(np.square(tx_taps2)) ** 0.5), \
        rx_taps2 / (np.sum(np.square(rx_taps2)) ** 0.5)


def plot_graphs(chan_sep, symb_sep, tx_taps, rx_taps=None):
    if rx_taps is None:
        rx_taps = tx_taps
    print_stats(tx_taps)
    print calc_ici(chan_sep, tx_taps, skip=0)
    print calc_ici2(chan_sep, tx_taps, rx_taps)
    print calc_isi(symb_sep, np.convolve(tx_taps, rx_taps))
    if not np.array_equal(tx_taps, rx_taps):
        print_stats(rx_taps)
    plot_spectrum(chan_sep, tx_taps)
    if not np.array_equal(tx_taps, rx_taps):
        plot_spectrum(chan_sep, rx_taps)
    plot_spectrum(chan_sep, np.convolve(tx_taps, rx_taps))
    plot_spectrum(symb_sep, np.convolve(tx_taps, rx_taps))
    plot_filters([tx_taps, rx_taps])
    print_taps(tx_taps)
    if not np.array_equal(tx_taps, rx_taps):
        print_taps(rx_taps)


def design1():
    chan_sep = 20
    symb_sep = 22
    nsr = 0.01
    # init_taps = get_fbmc_taps2(chan_sep, symb_sep)
    init_taps = get_rrc_taps(chan_sep, symb_sep, length=49 * 4 + 1)
    print_stats(init_taps)
    print calc_ici(chan_sep, init_taps)
    print calc_isi(symb_sep, np.convolve(init_taps, init_taps))
    print calc_nsr(chan_sep, init_taps, init_taps, nsr=nsr)
    # plot_spectrum(chan_sep, init_taps)

    taps = optimize1(chan_sep, symb_sep, init_taps,
                     skip=10, nsr=nsr, ici1_weight=100, ici2_weight=1)
    plot_graphs(chan_sep, symb_sep, taps)


def design2():
    chan_sep = 20
    symb_sep = 22
    nsr = 0.01
    # init_taps = get_fbmc_taps2(chan_sep, symb_sep)
    init_taps = get_rrc_taps(chan_sep, symb_sep, length=88 * 4 - 1)
    print_stats(init_taps)
    print calc_ici(chan_sep, init_taps)
    print calc_isi(symb_sep, np.convolve(init_taps, init_taps))
    print calc_nsr(chan_sep, init_taps, init_taps, nsr=nsr)
    # plot_spectrum(chan_sep, init_taps)

    tx_taps, rx_taps = init_taps, init_taps
    tx_taps, rx_taps = optimize4(
        chan_sep, symb_sep, tx_taps, rx_taps, skip1=4,
        ici1_weight=1, ici2_weight=100, isi1_weight=0.07)
    plot_graphs(chan_sep, symb_sep, tx_taps, rx_taps)


if __name__ == "__main__":
    # design1()
    # design2()
    # plot_graphs(20, 24, get_fbmc_taps2(20, 24))
    print calc_papr(2, [0.0, 1, 0.0])
