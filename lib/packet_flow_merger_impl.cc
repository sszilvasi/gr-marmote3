/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_flow_merger_impl.h"
#include <boost/thread/locks.hpp>
#include <gnuradio/block_detail.h>
#include <gnuradio/buffer.h>
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <marmote3/marmote3.pb.h>
#include <sstream>

namespace gr {
namespace marmote3 {

packet_flow_merger::sptr packet_flow_merger::make(int itemsize, int nflows,
                                                  int max_output_len) {
  return gnuradio::get_initial_sptr(
      new packet_flow_merger_impl(itemsize, nflows, max_output_len));
}

packet_flow_merger_impl::packet_flow_merger_impl(int itemsize, int nflows,
                                                 int max_output_len)
    : gr::block("packet_flow_merger",
                gr::io_signature::make(nflows, nflows, itemsize),
                gr::io_signature::make(1, 1, itemsize)),
      itemsize(itemsize), nflows(nflows), max_output_len(max_output_len),
      input_lens(nflows, 1), done(0) {
  if (max_output_len <= 0)
    throw std::invalid_argument(
        "packet_flow_merger: invalid maximum output length");

  if (itemsize <= 0)
    throw std::invalid_argument("packet_flow_merger: invalid item size");

  if (nflows <= 0)
    throw std::invalid_argument(
        "packet_flow_merger: invalid number of input flows");

  packets.resize(nflows);

  set_tag_propagation_policy(TPP_DONT);
  set_min_noutput_items(
      max_output_len); // THIS WAS THE BUG, OR AT LEAST FIXES IT
  set_min_output_buffer(0, 2 * max_output_len);
}

packet_flow_merger_impl::~packet_flow_merger_impl() {}

void packet_flow_merger_impl::forecast(int noutput_items,
                                       gr_vector_int &ninput_items_required) {
  bool progress = false;
  bool required = false;
  for (int i = 0; i < ninput_items_required.size(); i++) {
    gr::buffer_reader *reader = detail()->input(i).get();
    boost::unique_lock<boost::mutex> guard(*reader->mutex());

    if (reader->items_available() > 0) {
      ninput_items_required[i] = input_lens[i];
      progress = true;
      required = true;
    } else if (reader->done()) {
      ninput_items_required[i] = 0;
    } else {
      // TODO: this is a hack (we are spinning)
      ninput_items_required[i] = 0;
      progress = true;
    }

    if (false) {
      std::stringstream msg;
      msg << "forecast nout:" << noutput_items << " port:" << i
          << " inlen:" << input_lens[i] << " read:" << reader->nitems_read()
          << " avil:" << reader->items_available() << " done:" << reader->done()
          << " req:" << ninput_items_required[i] << " rd0:" << nitems_read(0)
          << " rd1:" << nitems_read(1) << " wr:" << nitems_written(0)
          << std::endl;
      std::cout << msg.str();
    }
  }

  if (!progress)
    done += 1;
  if (!required)
    boost::this_thread::sleep_for(boost::chrono::microseconds(50));
}

int packet_flow_merger_impl::general_work(
    int noutput_items, gr_vector_int &ninput_items,
    gr_vector_const_void_star &input_items, gr_vector_void_star &output_items) {
  if (false) {
    std::stringstream msg;
    msg << "gen_work rd0:" << nitems_read(0) << " rd1:" << nitems_read(1)
        << " wr:" << nitems_written(0) << " done:" << done
        << " nout:" << noutput_items << " nin0:" << ninput_items[0]
        << " nin1:" << ninput_items[1] << std::endl;
    std::cout << msg.str();
  }

  int port = -1;
  pmt::pmt_t srcid;
  pmt::pmt_t dict;
  int best_ninput_items = 0;

  std::vector<tag_t> tags;
  for (int i = 0; i < ninput_items.size(); i++) {
    if (ninput_items[i] <= 0)
      continue;

    int r = nitems_read(i);
    int c = ninput_items[i];

    // get the length and dictionaries
    get_tags_in_range(tags, i, r, r + 1, PMT_PACKET_LEN);
    if (tags.size() == 1 && pmt::is_integer(tags[0].value)) {
      input_lens[i] = std::max(1, (int)pmt::to_long(tags[0].value));
      if (c < input_lens[i] || c <= best_ninput_items)
        continue;

      best_ninput_items = c;
      port = i;
      srcid = tags[0].srcid;

      get_tags_in_range(tags, i, r, r + 1, PMT_PACKET_DICT);
      if (tags.size() == 1 && pmt::is_dict(tags[0].value)) {
        dict = tags[0].value;
      } else {
        dict = PMT_EMPTY_DICT;
        // drop the dictionary on error
        if (tags.size() != 0)
          std::cout
              << "####### packet_flow_merger: invalid packet dictionary\n";
      }
    } else {
      // skip input items on missing packet length
      std::cout << "####### packet_flow_merger: missing packet length\n";

      get_tags_in_range(tags, 0, r + 1, r + ninput_items[i]);
      if (tags.size() > 0)
        c = std::max(1, (int)(tags[0].offset - r));

      consume(i, c);
      continue;
    }
  }

  if (0 <= port && port < nflows) {
    int input_len = input_lens[port];
    std::memcpy(output_items[0], input_items[port], input_len * itemsize);
    add_item_tag(0, nitems_written(0), PMT_PACKET_LEN,
                 pmt::from_long(input_len), srcid);
    add_item_tag(0, nitems_written(0), PMT_PACKET_DICT, dict, srcid);

    if (false) {
      std::stringstream msg;
      msg << "produced port:" << port << " len:" << input_len
          << " rd0:" << nitems_read(0) << " rd1:" << nitems_read(1)
          << " wr:" << nitems_written(0) << std::endl;
      std::cout << msg.str();
    }

    {
      gr::thread::scoped_lock lock(mutex);
      packets[port] += 1;
    }

    consume(port, input_len);
    produce(0, input_len);
    input_lens[port] = 1;
  }

  if (done > 0) {
    std::stringstream msg;
    msg << alias() << ": work done b/c no more input\n";
    std::cout << msg.str();
    return WORK_DONE;
  }

  return WORK_CALLED_PRODUCE;
}

void packet_flow_merger_impl::collect_block_report(ModemReport *modem_report) {
  PacketFlowReport *report = modem_report->mutable_packet_flow_merger();
  gr::thread::scoped_lock lock(mutex);

  *report->mutable_packets() = {packets.begin(), packets.end()};

  for (int i = 0; i < packets.size(); i++)
    packets[i] = 0;
}

} /* namespace marmote3 */
} /* namespace gr */
