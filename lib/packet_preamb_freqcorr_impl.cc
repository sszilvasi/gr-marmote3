/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_preamb_freqcorr_impl.h"
#include <gnuradio/fft/window.h>
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <volk/volk.h>

namespace gr {
namespace marmote3 {

packet_preamb_freqcorr::sptr packet_preamb_freqcorr::make(int preamb_start,
                                                          int preamb_length,
                                                          int block_length,
                                                          int block_stride,
                                                          int max_output_len) {
  return gnuradio::get_initial_sptr(new packet_preamb_freqcorr_impl(
      preamb_start, preamb_length, block_length, block_stride, max_output_len));
}

packet_preamb_freqcorr_impl::packet_preamb_freqcorr_impl(int preamb_start,
                                                         int preamb_length,
                                                         int block_length,
                                                         int block_stride,
                                                         int max_output_len)
    : tagged_stream_block2("packet_preamb_freqcorr",
                           gr::io_signature::make(1, 1, sizeof(gr_complex)),
                           gr::io_signature::make(1, 1, sizeof(gr_complex)),
                           max_output_len),
      preamb_start(preamb_start), preamb_length(preamb_length),
      block_length(block_length), block_stride(block_stride),
      fir(1, gr::fft::window::hamming(block_length)) {
  if (preamb_start < 0)
    throw std::invalid_argument(
        "packet_preamb_freqcorr: preamble start cannot be negative");

  if (block_length <= 0)
    throw std::invalid_argument(
        "packet_preamb_freqcorr: block length must be positive");

  if (block_stride <= 0)
    throw std::invalid_argument(
        "packet_preamb_freqcorr: block stride must be positive");

  if (preamb_length < block_length + block_stride)
    throw std::invalid_argument(
        "packet_preamb_freqcorr: preamble length is too small");

  if (max_output_len < preamb_start + preamb_length)
    throw std::invalid_argument(
        "packet_preamb_freqcorr: maximum output length is too small");
}

packet_preamb_freqcorr_impl::~packet_preamb_freqcorr_impl() {}

int packet_preamb_freqcorr_impl::work(int noutput_items,
                                      gr_vector_int &ninput_items,
                                      gr_vector_const_void_star &input_items,
                                      gr_vector_void_star &output_items) {
  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];
  int len = ninput_items[0];

  if (preamb_start + preamb_length > len) {
    std::cout << "packet_preamb_freqcorr: ####### input packet is too short\n";
    return 0;
  }

  gr_complex a(0.0f, 0.0f);

  gr_complex c = fir.filter(in + preamb_start);
  for (int n = block_stride; n + block_length <= preamb_length;
       n += block_stride) {
    gr_complex d = fir.filter(in + preamb_start + n);
    a += std::conj(c) * d;
    c = d;
  }

  float freq_err = std::arg(a) / block_stride;
  set_output_float(PMT_PREAMB_FREQ, freq_err);

  gr_complex offset(std::cos(-freq_err), std::sin(-freq_err));
  gr_complex start(1.0f, 0.0f);
  volk_32fc_s32fc_x2_rotator_32fc(out, in, offset, &start, len);

  return len;
}

} /* namespace marmote3 */
} /* namespace gr */
