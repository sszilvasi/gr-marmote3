/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "zmq_packet_sink_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/constants.h>
#include <sstream>

namespace gr {
namespace marmote3 {

zmq_packet_sink::sptr
zmq_packet_sink::make(int itemsize, const std::string &addr, bool push) {
  return gnuradio::get_initial_sptr(
      new zmq_packet_sink_impl(itemsize, addr, push));
}

zmq_packet_sink_impl::zmq_packet_sink_impl(int itemsize,
                                           const std::string &addr, bool push)
    : tagged_stream_block2("zmq_packet_sink",
                           gr::io_signature::make(1, 1, sizeof(itemsize)),
                           gr::io_signature::make(0, 0, 0), 0),
      itemsize(itemsize), addr(addr), context(1),
      socket(context, push ? ZMQ_PUSH : ZMQ_PUB), success_count(0),
      failure_count(0) {
  if (itemsize != 1 && itemsize != 4 && itemsize != 8)
    throw std::out_of_range("zmq_packet_sink: incorrect item size");

  // will this throw an error?
  socket.bind(addr.c_str());
  std::cout << "zmq_packet_sink: sending on " << addr << std::endl;

  last_report = std::chrono::system_clock::now();
}

zmq_packet_sink_impl::~zmq_packet_sink_impl() { socket.close(); }

int zmq_packet_sink_impl::work(int noutput_items, gr_vector_int &ninput_items,
                               gr_vector_const_void_star &input_items,
                               gr_vector_void_star &output_items) {
  const unsigned char *in = (const unsigned char *)input_items[0];

  pmt::pmt_t payload;
  if (itemsize == 1)
    payload = pmt::init_u8vector(ninput_items[0], in);
  else if (itemsize == 4)
    payload = pmt::init_f32vector(ninput_items[0], (const float *)in);
  else if (itemsize == 8)
    payload = pmt::init_c32vector(ninput_items[0], (const gr_complex *)in);
  else
    payload = PMT_NULL; // this should not happen

  // TODO: Do we really need this many copies?
  pmt::pmt_t msg1 = pmt::cons(output_dict, payload);
  std::string msg2 = pmt::serialize_str(msg1);
  zmq::message_t msg3(msg2.size());
  std::memcpy(msg3.data(), msg2.c_str(), msg2.size());

  if (socket.send(msg3))
    success_count += 1;
  else
    failure_count += 1;

  std::chrono::system_clock::time_point current =
      std::chrono::system_clock::now();
  std::chrono::duration<float> elapsed = current - last_report;
  if (elapsed.count() >= 5.0f) {
    std::stringstream msg;
    msg << "zmq_packet_sink: sent " << ((float)success_count / elapsed.count())
        << " msg/s [num:" << success_count << " err:" << failure_count << "]"
        << std::endl;
    std::cout << msg.str();
    success_count = 0;
    failure_count = 0;
    last_report = current;
  }

  return 0;
}

} /* namespace marmote3 */
} /* namespace gr */
