/* -*- c++ -*- */
/*
 * Copyright 2018 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_MODEM_MONITOR_IMPL_H
#define INCLUDED_MARMOTE3_MODEM_MONITOR_IMPL_H

#include <marmote3/modem_monitor.h>
#include <vector>
#include <zmq.hpp>

namespace gr {
namespace marmote3 {

class modem_monitor_impl : public modem_monitor {
private:
  const bool enable_perf_report;

  std::vector<modem_monitored *> monitored_list;
  gr::thread::mutex monitored_mutex;

  unsigned int sequence_no;

  zmq::context_t context;

  const int report_rate;
  const std::string report_addr;
  const int print_mode;
  zmq::socket_t report_socket;
  boost::shared_ptr<boost::thread> report_thread;
  void report_work();

  const std::string command_addr;
  zmq::socket_t command_socket;
  boost::shared_ptr<boost::thread> command_thread;
  void command_work();

  static double get_time();

public:
  modem_monitor_impl(int report_rate, const std::string &report_addr,
                     int print_mode, const std::string &command_addr);
  ~modem_monitor_impl();

  void register_monitored(modem_monitored *monitored) override;
  void unregister_monitored(modem_monitored *monitored) override;

  bool start();
  bool stop();

  void collect_modem_report(ModemReport &modem_report);
  std::string collect_serialized_report() override;
  void process_serialized_command(const std::string &command) override;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_MODEM_MONITOR_IMPL_H */
