/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <marmote3/constants.h>
#include <marmote3/packet_symb_mod.h>

namespace gr {
namespace marmote3 {

const pmt::pmt_t PMT_NULL = pmt::get_PMT_NIL();
const pmt::pmt_t PMT_PACKET_LEN = pmt::string_to_symbol("packet_len");
const pmt::pmt_t PMT_PACKET_DICT = pmt::string_to_symbol("packet_dict");
const pmt::pmt_t PMT_EMPTY_DICT = pmt::make_dict();
const pmt::pmt_t PMT_PACKET_SRC = pmt::string_to_symbol("packet_src");
const pmt::pmt_t PMT_PACKET_DST = pmt::string_to_symbol("packet_dst");
const pmt::pmt_t PMT_PACKET_ID = pmt::string_to_symbol("packet_id");
const pmt::pmt_t PMT_PACKET_MCS = pmt::string_to_symbol("packet_mcs");
const pmt::pmt_t PMT_DATA_LEN = pmt::string_to_symbol("data_len");
const pmt::pmt_t PMT_CHANNEL_NUM = pmt::string_to_symbol("channel_num");
const pmt::pmt_t PMT_TRIG_SAMP = pmt::string_to_symbol("trig_samp");
const pmt::pmt_t PMT_TRIG_TIME = pmt::string_to_symbol("trig_time");
const pmt::pmt_t PMT_PREAMB_FREQ = pmt::string_to_symbol("preamb_freq");
const pmt::pmt_t PMT_FREQ_ERR = pmt::string_to_symbol("freq_err");
const pmt::pmt_t PMT_PHASE_ERR = pmt::string_to_symbol("phase_err");
const pmt::pmt_t PMT_EQU_TAPS = pmt::string_to_symbol("equ_taps");
const pmt::pmt_t PMT_EQU_PWR = pmt::string_to_symbol("equ_pwr");
const pmt::pmt_t PMT_EQU_SNR = pmt::string_to_symbol("equ_snr");
const pmt::pmt_t PMT_LOAD_TIME = pmt::string_to_symbol("load_time");
const pmt::pmt_t PMT_SCHD_TIME = pmt::string_to_symbol("schd_time");
const pmt::pmt_t PMT_FLOW_ID = pmt::string_to_symbol("flow_id");

int get_modulation_order(modulation_t mod) {
  switch (mod) {
  case MOD_QPSK:
    return 2;
  case MOD_8PSK:
    return 3;
  case MOD_16QAM:
    return 4;
  case MOD_64QAM:
    return 6;
  default:
    return 1;
  }
}

int mcscheme_t::get_code_len(int data_len) const {
  return 6 * (int)std::round((1.0f / 6) * data_len / rate);
}

int mcscheme_t::get_symb_len(int data_len) const {
  return 8 * get_code_len(data_len) / get_modulation_order(mod);
}

const mcscheme_t MCSCHEME_TAB[MCSCHEME_NUM] = {
    {MOD_BPSK, 0.5f, 0.0f},       // symb/byte = 16
    {MOD_BPSK, 0.625f, 0.0f},     // symb/byte = 12.8
    {MOD_BPSK, 0.8f, 0.0f},       // symb/byte = 10
    {MOD_QPSK, 0.625f, 0.0f},     // symb/byte = 6.4
    {MOD_QPSK, 0.8f, 0.0f},       // symb/byte = 5
    {MOD_16QAM, 0.5f, 0.0f},      // symb/byte = 4
    {MOD_16QAM, 0.625f, 0.0f},    // symb/byte = 3.2
    {MOD_16QAM, 0.75f, 0.0f},     // symb_byte = 2.666
    {MOD_16QAM, 0.875f, 0.0f},    // symb/byte = 2.28
    {MOD_64QAM, 0.666666f, 0.0f}, // symb/byte = 2
    {MOD_64QAM, 0.75f, 0.0f},     // symb/byte = 1.777
    {MOD_64QAM, 0.75f, -2.0f},    // symb/byte = 1.777
};

const mcscheme_t &get_mcscheme(int index) {
  if (index < 0)
    index = 0;
  else if (index >= MCSCHEME_NUM)
    index = MCSCHEME_NUM - 1;

  return MCSCHEME_TAB[index];
}

} /* namespace marmote3 */
} /* namespace gr */
