#include "fading_generator_correlated.hpp"

#include <cassert>
#include <iostream>

using namespace std;

namespace mimoradio  {

    fading_generator_correlated::fading_generator_correlated(double norm_doppler)
    {
        this->norm_doppler = norm_doppler;
    }
    
    fading_generator_correlated::~fading_generator_correlated()
    {
        
    }
    
    void fading_generator_correlated::set_norm_doppler (double norm_doppler)
    {
        assert(norm_doppler > 0.0);
        this->norm_doppler = norm_doppler;
        cout << this->norm_doppler << endl;
    }

    void fading_generator_correlated::set_doppler_spectrum(DOPPLER_SPECTRUM spectrum)
    {
        doppler_spectrum = spectrum;
    }

}