/* -*- c++ -*- */
/*
 * Copyright 2013-2018 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "packet_symb_demod_impl.h"
#include <emmintrin.h>
#include <gnuradio/io_signature.h>
#include <pmmintrin.h>
#include <smmintrin.h>
#include <xmmintrin.h>

namespace gr {
namespace marmote3 {

int decode_bpsk(const gr_complex *symbs, int len, float scaling,
                int8_t *sbits) {
  if (len % 16 != 0) {
    std::cout << "####### decode_bpsk: invalid number of symbols\n";
    return 0;
  }

  __m128 xx = _mm_set1_ps(scaling);
  for (int i = 0; i < len; i += 16) {
    __m128 as = _mm_shuffle_ps(_mm_load_ps((float *)symbs),
                               _mm_load_ps((float *)(symbs + 2)),
                               _MM_SHUFFLE(2, 0, 2, 0));
    __m128 bs = _mm_shuffle_ps(_mm_load_ps((float *)(symbs + 4)),
                               _mm_load_ps((float *)(symbs + 6)),
                               _MM_SHUFFLE(2, 0, 2, 0));
    __m128 cs = _mm_shuffle_ps(_mm_load_ps((float *)(symbs + 8)),
                               _mm_load_ps((float *)(symbs + 10)),
                               _MM_SHUFFLE(2, 0, 2, 0));
    __m128 ds = _mm_shuffle_ps(_mm_load_ps((float *)(symbs + 12)),
                               _mm_load_ps((float *)(symbs + 14)),
                               _MM_SHUFFLE(2, 0, 2, 0));

    as = _mm_mul_ps(as, xx);
    bs = _mm_mul_ps(bs, xx);
    cs = _mm_mul_ps(cs, xx);
    ds = _mm_mul_ps(ds, xx);

    __m128i ai = _mm_cvtps_epi32(as);
    __m128i bi = _mm_cvtps_epi32(bs);
    __m128i ci = _mm_cvtps_epi32(cs);
    __m128i di = _mm_cvtps_epi32(ds);

    ai = _mm_packs_epi32(ai, bi);
    ci = _mm_packs_epi32(ci, di);
    ai = _mm_packs_epi16(ai, ci);

    _mm_store_si128((__m128i *)sbits, ai);

    symbs += 16;
    sbits += 16;
  }

  return len;
}

int decode_qpsk(const gr_complex *symbs, int len, float scaling,
                int8_t *sbits) {
  if (len % 8 != 0) {
    std::cout << "####### decode_qpsk: invalid number of symbols\n";
    return 0;
  }

  __m128 xx = _mm_set1_ps(scaling);
  for (int i = 0; i < len; i += 8) {
    __m128 as = _mm_load_ps((float *)symbs);
    __m128 bs = _mm_load_ps((float *)(symbs + 2));
    __m128 cs = _mm_load_ps((float *)(symbs + 4));
    __m128 ds = _mm_load_ps((float *)(symbs + 6));

    as = _mm_mul_ps(as, xx);
    bs = _mm_mul_ps(bs, xx);
    cs = _mm_mul_ps(cs, xx);
    ds = _mm_mul_ps(ds, xx);

    __m128i ai = _mm_cvtps_epi32(as);
    __m128i bi = _mm_cvtps_epi32(bs);
    __m128i ci = _mm_cvtps_epi32(cs);
    __m128i di = _mm_cvtps_epi32(ds);

    ai = _mm_packs_epi32(ai, bi);
    ci = _mm_packs_epi32(ci, di);
    ai = _mm_packs_epi16(ai, ci);

    _mm_store_si128((__m128i *)sbits, ai);

    symbs += 8;
    sbits += 16;
  }

  return 2 * len;
}

int decode_8psk(const gr_complex *symbs, int len, float scaling,
                int8_t *sbits) {
  if (len % 4 != 0) {
    std::cout << "####### decode_8psk: invalid number of symbols\n";
    return 0;
  }

  __m128 xx = _mm_set1_ps(scaling);
  __m128 abs_mask = _mm_set1_ps(-0.0f); // only highest bit set
  __m128i perm_mask =
      _mm_set_epi8(-1, -1, -1, -1, 7, 6, 11, 5, 4, 10, 3, 2, 9, 1, 0, 8);
  __m128i store_mask =
      _mm_set_epi8(0, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);

  for (int i = 0; i < len; i += 4) {
    __m128 s0 = _mm_load_ps((float *)symbs);       // r0, i0, r1, i1
    __m128 s1 = _mm_load_ps((float *)(symbs + 2)); // r2, i2, r3, i3
    s0 = _mm_mul_ps(s0, xx);
    s1 = _mm_mul_ps(s1, xx);
    __m128 p0 = _mm_andnot_ps(abs_mask, s0); // abs values
    __m128 p1 = _mm_andnot_ps(abs_mask, s1); // abs values
    __m128 p2 = _mm_hsub_ps(p0, p1);         // d0, d1, d2, d3

    __m128i ai = _mm_cvtps_epi32(s0);
    __m128i bi = _mm_cvtps_epi32(s1);
    __m128i ci = _mm_cvtps_epi32(p2);

    ai = _mm_packs_epi32(ai, bi);
    ci = _mm_packs_epi32(ci, ci);
    ai = _mm_packs_epi16(ai, ci);

    ai = _mm_shuffle_epi8(ai, perm_mask);
    _mm_maskmoveu_si128(ai, store_mask, (char *)sbits);

    symbs += 4;
    sbits += 12;
  }

  return 3 * len;
}

const float sqrt_four_tenth = std::sqrt(4.0f / 10);

int decode_16qam(const gr_complex *symbs, int len, float scaling,
                 int8_t *sbits) {
  if (len % 4 != 0) {
    std::cout << "####### decode_16qam: invalid number of symbols\n";
    return 0;
  }

  __m128 xx = _mm_set1_ps(scaling);
  __m128 md = _mm_set1_ps(sqrt_four_tenth * scaling); // midpoint
  __m128 mask = _mm_set1_ps(-0.0f);                   // only highest bit set

  for (int i = 0; i < len; i += 4) {
    __m128 s0 = _mm_load_ps((float *)symbs);       // r0, i0, r1, i1
    __m128 s1 = _mm_load_ps((float *)(symbs + 2)); // r2, i2, r3, i3
    s0 = _mm_mul_ps(s0, xx);
    s1 = _mm_mul_ps(s1, xx);
    __m128 p0 = _mm_sub_ps(_mm_andnot_ps(mask, s0), md); // abs - mid
    __m128 p1 = _mm_sub_ps(_mm_andnot_ps(mask, s1), md); // abs - mid

    __m128 as = _mm_unpacklo_ps(p0, s0);
    __m128 bs = _mm_unpackhi_ps(p0, s0);
    __m128 cs = _mm_unpacklo_ps(p1, s1);
    __m128 ds = _mm_unpackhi_ps(p1, s1);

    __m128i ai = _mm_cvtps_epi32(as);
    __m128i bi = _mm_cvtps_epi32(bs);
    __m128i ci = _mm_cvtps_epi32(cs);
    __m128i di = _mm_cvtps_epi32(ds);

    ai = _mm_packs_epi32(ai, bi);
    ci = _mm_packs_epi32(ci, di);
    ai = _mm_packs_epi16(ai, ci);

    _mm_store_si128((__m128i *)sbits, ai);

    symbs += 4;
    sbits += 16;
  }

  return 4 * len;
}

const float unit_64qam_1 = std::sqrt(1.0f / 42);
const float unit_64qam_2 = 2.0f * unit_64qam_1;
const float unit_64qam_4 = 4.0f * unit_64qam_1;

int decode_64qam(const gr_complex *symbs, int len, float scaling,
                 int8_t *sbits) {
  if (len % 2 != 0) {
    std::cout << "####### decode_64qam: invalid number of symbols\n";
    return 0;
  }

  __m128 xx = _mm_set1_ps(scaling);
  __m128 u2 = _mm_set1_ps(unit_64qam_2 * scaling);
  __m128 u4 = _mm_set1_ps(unit_64qam_4 * scaling);
  __m128 abs_mask = _mm_set1_ps(-0.0f); // only highest bit set
  __m128i perm_mask =
      _mm_set_epi8(-1, -1, -1, -1, 3, 7, 11, 2, 6, 10, 1, 5, 9, 0, 4, 8);
  __m128i store_mask =
      _mm_set_epi8(0, 0, 0, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1);

  for (int i = 0; i < len; i += 2) {
    __m128 a = _mm_mul_ps(_mm_load_ps((float *)symbs), xx); // r0, i0, r1, i1
    __m128 b = _mm_sub_ps(_mm_andnot_ps(abs_mask, a), u4);  // 4-abs(a)
    __m128 c = _mm_sub_ps(_mm_andnot_ps(abs_mask, b), u2);  // 2-abs(b)

    __m128i ai = _mm_cvtps_epi32(a);
    __m128i bi = _mm_cvtps_epi32(b);
    __m128i ci = _mm_cvtps_epi32(c);

    ai = _mm_packs_epi32(ai, bi);
    ci = _mm_packs_epi32(ci, ci);
    ai = _mm_packs_epi16(ai, ci);

    ai = _mm_shuffle_epi8(ai, perm_mask);
    _mm_maskmoveu_si128(ai, store_mask, (char *)sbits);

    symbs += 2;
    sbits += 12;
  }

  return 6 * len;
}

packet_symb_demod::sptr packet_symb_demod::make(modulation_t mod, float scaling,
                                                int max_output_len) {
  return gnuradio::get_initial_sptr(
      new packet_symb_demod_impl(mod, scaling, max_output_len));
}

packet_symb_demod_impl::packet_symb_demod_impl(modulation_t mod, float scaling,
                                               int max_output_len)
    : tagged_stream_block2(
          "packet_symb_demod", gr::io_signature::make(1, 1, sizeof(gr_complex)),
          gr::io_signature::make(1, 1, sizeof(unsigned char)), max_output_len),
      mod(mod), scaling(scaling) {
  if (mod != MOD_BPSK && mod != MOD_QPSK && mod != MOD_8PSK &&
      mod != MOD_16QAM && mod != MOD_64QAM && mod != MOD_MCS)
    throw std::invalid_argument("packet_symb_demod: invalid modulation");
}

packet_symb_demod_impl::~packet_symb_demod_impl() {}

int packet_symb_demod_impl::work(int noutput_items, gr_vector_int &ninput_items,
                                 gr_vector_const_void_star &input_items,
                                 gr_vector_void_star &output_items) {
  modulation_t mod2 = mod;
  if (mod2 == MOD_MCS) {
    if (!has_input_long(0, PMT_PACKET_MCS)) {
      std::cout << "####### packet_symb_demod: MCS is not set\n";
      return 0;
    }
    mod2 = get_mcscheme(get_input_long(0, PMT_PACKET_MCS, -1)).mod;
  }

  int len = ninput_items[0];
  if (len * get_modulation_order(mod2) > noutput_items) {
    std::cout << "####### packet_symb_demod: output buffer too short\n";
    return 0;
  }

  const gr_complex *symbs = (const gr_complex *)input_items[0];
  int8_t *sbits = (int8_t *)output_items[0];

  int len2;
  switch (mod2) {
  case MOD_BPSK:
    len2 = decode_bpsk(symbs, len, scaling, sbits);
    break;
  case MOD_QPSK:
    len2 = decode_qpsk(symbs, len, scaling, sbits);
    break;
  case MOD_8PSK:
    len2 = decode_8psk(symbs, len, scaling, sbits);
    break;
  case MOD_16QAM:
    len2 = decode_16qam(symbs, len, scaling, sbits);
    break;
  case MOD_64QAM:
    len2 = decode_64qam(symbs, len, scaling, sbits);
    break;
  default:
    std::cout << "####### packet_symb_demod: invalid modulation\n";
    return 0;
  }

  return len2;
}

} /* namespace marmote3 */
} /* namespace gr */
