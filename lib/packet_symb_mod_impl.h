/* -*- c++ -*- */
/*
 * Copyright 2013-2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_PACKET_SYMB_MOD_IMPL_H
#define INCLUDED_MARMOTE3_PACKET_SYMB_MOD_IMPL_H

#include <marmote3/packet_symb_mod.h>

namespace gr {
namespace marmote3 {

extern const gr_complex points_bpsk[2];
int encode_bpsk(const unsigned char *bytes, int len, gr_complex *symbs);

extern const gr_complex points_qpsk[4];
int encode_qpsk(const unsigned char *bytes, int len, gr_complex *symbs);

extern const gr_complex points_8psk[8];
int encode_8psk(const unsigned char *bytes, int len, gr_complex *symbs);

extern const gr_complex points_16qam[16];
int encode_16qam(const unsigned char *bytes, int len, gr_complex *symbs);

extern const gr_complex points_64qam[64];
int encode_64qam(const unsigned char *bytes, int len, gr_complex *symbs);

class packet_symb_mod_impl : public packet_symb_mod {
private:
  const modulation_t mod;

public:
  packet_symb_mod_impl(modulation_t mod, int max_output_len);
  ~packet_symb_mod_impl();

  int work(int noutput_items, gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_PACKET_SYMB_MOD_IMPL_H */
