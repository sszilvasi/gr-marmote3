/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_PACKET_PREAMB_INSERT_IMPL_H
#define INCLUDED_MARMOTE3_PACKET_PREAMB_INSERT_IMPL_H

#include <marmote3/constants.h>
#include <marmote3/fec_conv_xcoder.h>
#include <marmote3/packet_preamb_insert.h>

namespace gr {
namespace marmote3 {

class packet_preamb_insert_impl : public packet_preamb_insert {
private:
  const std::vector<gr_complex> preamble;
  const bool mcs_config;
  float power;

  const header_t header;
  const fec_conv_encoder encoder;
  const bool postamble;

public:
  packet_preamb_insert_impl(const std::vector<gr_complex> &preamble,
                            bool mcs_config, float power, header_t header,
                            bool postamble, int max_output_len);
  ~packet_preamb_insert_impl();

  int work(int noutput_items, gr_vector_int &ninput_items,
           gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);

  void set_power(float power) override;
  float get_power() const override;
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_PACKET_PREAMB_INSERT_IMPL_H */
