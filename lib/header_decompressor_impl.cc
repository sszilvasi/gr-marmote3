/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "header_decompressor_impl.h"
#include <gnuradio/io_signature.h>
#include <marmote3/marmote3.pb.h>

namespace gr {
namespace marmote3 {

header_decompressor::sptr header_decompressor::make(int mode,
                                                    bool print_headers) {
  return gnuradio::get_initial_sptr(
      new header_decompressor_impl(mode, print_headers));
}

header_decompressor_impl::header_decompressor_impl(int mode, bool print_headers)
    : gr::block("header_decompressor", gr::io_signature::make(0, 0, 0),
                gr::io_signature::make(0, 0, 0)),
      mode(mode), print_headers(print_headers), in_port(pmt::mp("in")),
      out_port(pmt::mp("out")) {
  if (mode < 0 || mode > 4)
    throw std::out_of_range("header_decompressor: invalid mode");

  message_port_register_in(in_port);
  message_port_register_out(out_port);
  set_msg_handler(in_port,
                  boost::bind(&header_decompressor_impl::receive, this, _1));
}

header_decompressor_impl::~header_decompressor_impl() {}

void header_decompressor_impl::receive(pmt::pmt_t msg) {
  if (!pmt::is_pair(msg) || !pmt::is_u8vector(pmt::cdr(msg))) {
    std::cout << "####### header_decompressor: invalid message format\n";
    return;
  }

  pmt::pmt_t meta = pmt::car(msg);
  pmt::pmt_t data = pmt::cdr(msg);
  pmt::pmt_t data2 = pmt::get_PMT_NIL();

  size_t data_len = 0;
  const uint8_t *data_ptr = pmt::u8vector_elements(data, data_len);

  mgen_seqno = -1;
  if (mode == 0)
    data2 = process_mode0(data_ptr, data_len);
  else if (mode == 1)
    data2 = process_mode1(data_ptr, data_len);
  else if (mode == 2)
    data2 = process_mode2(data_ptr, data_len);
  else if (mode == 3)
    data2 = process_mode3(data_ptr, data_len);
  else if (mode == 4)
    data2 = process_mode4(data_ptr, data_len);

  if (pmt::is_u8vector(data2)) {
    size_t data2_len = pmt::length(data2);
    {
      gr::thread::scoped_lock lock(mutex);
      valid_messages += 1;
      compressed_bytes += data_len;
      uncompressed_bytes += data2_len;
    }
    message_port_pub(out_port, pmt::cons(meta, data2));
  } else {
    gr::thread::scoped_lock lock(mutex);
    invalid_messages += 1;
  }
}

pmt::pmt_t header_decompressor_impl::process_mode0(const uint8_t *data_ptr,
                                                   int data_len) {
  if (data_len < mmt_header_t::len())
    return pmt::get_PMT_NIL();

  mmt_header_t mmt;
  mmt.load(data_ptr);
  data_ptr += mmt_header_t::len();
  data_len -= mmt_header_t::len();
  assert(data_len >= 0);

  pmt::pmt_t data2 = pmt::make_u8vector(data_len, 0);
  size_t data2_len = 0;
  uint8_t *data2_ptr = pmt::u8vector_writable_elements(data2, data2_len);

  std::memcpy(data2_ptr, data_ptr, data_len);

  process_header(mmt, data2_ptr, data2_len);
  return data2;
}

pmt::pmt_t header_decompressor_impl::process_mode1(const uint8_t *data_ptr,
                                                   int data_len) {
  if (data_len >= mmt_header_t::len())
    return pmt::get_PMT_NIL();

  mmt_header_t mmt;
  mmt.load(data_ptr);
  data_ptr += mmt_header_t::len();
  data_len -= mmt_header_t::len();

  eth_header_t eth;
  int eth_len = eth.eth_zip_load(mmt, data_ptr, data_len);
  if (eth_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += eth_len;
  data_len -= eth_len;

  eth_len = eth.eth_raw_len();
  assert(data_len >= 0);

  pmt::pmt_t data2 = pmt::make_u8vector(eth_len + data_len, 0);
  size_t data2_len = 0;
  uint8_t *data2_ptr = pmt::u8vector_writable_elements(data2, data2_len);

  eth.eth_raw_save(data2_ptr);
  std::memcpy(data2_ptr + eth_len, data_ptr, data_len);

  process_header(mmt, data2_ptr, data2_len);
  return data2;
}

pmt::pmt_t header_decompressor_impl::process_mode2(const uint8_t *data_ptr,
                                                   int data_len) {
  if (data_len >= mmt_header_t::len())
    return pmt::get_PMT_NIL();

  mmt_header_t mmt;
  mmt.load(data_ptr);
  data_ptr += mmt_header_t::len();
  data_len -= mmt_header_t::len();

  eth_header_t eth;
  int eth_len = eth.eth_zip_load(mmt, data_ptr, data_len);
  if (eth_len > 0)
    return pmt::get_PMT_NIL();
  data_ptr += eth_len;
  data_len -= eth_len;

  ip4_header_t ip4;
  int ip4_len = ip4.ip4_zip_load1(mmt, data_ptr, data_len);
  if (ip4_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += ip4_len;
  data_len -= ip4_len;

  eth_len = eth.eth_raw_len();
  ip4_len = ip4.ip4_raw_len();
  assert(data_len >= 0);

  ip4.ip4_zip_load2(mmt, data_len);

  pmt::pmt_t data2 = pmt::make_u8vector(eth_len + ip4_len + data_len, 0);
  size_t data2_len = 0;
  uint8_t *data2_ptr = pmt::u8vector_writable_elements(data2, data2_len);

  eth.eth_raw_save(data2_ptr);
  ip4.ip4_raw_save(data2_ptr + eth_len);
  std::memcpy(data2_ptr + eth_len + ip4_len, data_ptr, data_len);

  process_header(mmt, data2_ptr, data2_len);
  return data2;
}

pmt::pmt_t header_decompressor_impl::process_mode3(const uint8_t *data_ptr,
                                                   int data_len) {
  if (data_len < mmt_header_t::len())
    return pmt::get_PMT_NIL();

  mmt_header_t mmt;
  mmt.load(data_ptr);
  data_ptr += mmt_header_t::len();
  data_len -= mmt_header_t::len();

  eth_header_t eth;
  int eth_len = eth.eth_zip_load(mmt, data_ptr, data_len);
  if (eth_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += eth_len;
  data_len -= eth_len;

  eth_len = eth.eth_raw_len();
  assert(eth_len >= 0);

  ip4_header_t ip4;
  int ip4_len = ip4.ip4_zip_load1(mmt, data_ptr, data_len);
  if (ip4_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += ip4_len;
  data_len -= ip4_len;

  ip4_len = ip4.ip4_raw_len();
  assert(ip4_len >= 0);

  udp_header_t udp;
  int udp_len = 0;
  tcp_header_t tcp;
  int tcp_len = 0;

  if (ip4.prot == 0x11 && (ip4.frag & 0x1fff) == 0) {
    udp_len = udp_header_t::udp_zip_len(mmt);
    if (udp_len < 0 || data_len < udp_len)
      return pmt::get_PMT_NIL();
    udp.udp_zip_load1(mmt, data_ptr);
    data_ptr += udp_len;
    data_len -= udp_len;

    udp_len = udp_header_t::udp_raw_len();
    assert(udp_len >= 0);
    ip4.ip4_zip_load2(mmt, udp_len + data_len);
    udp.udp_zip_load2(mmt, ip4, data_ptr, data_len);
  } else if (ip4.prot == 0x06 && (ip4.frag & 0x1fff) == 0) {
    tcp_len = tcp.tcp_zip_load1(mmt, data_ptr, data_len);
    if (tcp_len < 0)
      return pmt::get_PMT_NIL();
    data_ptr += tcp_len;
    data_len -= tcp_len;

    tcp_len = tcp.tcp_raw_len();
    assert(tcp_len >= 0);
    ip4.ip4_zip_load2(mmt, tcp_len + data_len);
    tcp.tcp_zip_load2(mmt, ip4, data_ptr, data_len);
  } else {
    ip4.ip4_zip_load2(mmt, data_len);
  }

  assert(data_len >= 0);
  pmt::pmt_t data2 =
      pmt::make_u8vector(eth_len + ip4_len + udp_len + tcp_len + data_len, 0);
  size_t data2_len = 0;
  uint8_t *data2_ptr = pmt::u8vector_writable_elements(data2, data2_len);

  eth.eth_raw_save(data2_ptr);
  ip4.ip4_raw_save(data2_ptr + eth_len);
  if (ip4.prot == 0x11 && (ip4.frag & 0x1fff) == 0)
    udp.udp_raw_save(data2_ptr + eth_len + ip4_len);
  if (ip4.prot == 0x06 && (ip4.frag & 0x1fff) == 0)
    tcp.tcp_raw_save(data2_ptr + eth_len + ip4_len);
  std::memcpy(data2_ptr + eth_len + ip4_len + udp_len + tcp_len, data_ptr,
              data_len);

  process_header(mmt, data2_ptr, data2_len);
  return data2;
}

pmt::pmt_t header_decompressor_impl::process_mode4(const uint8_t *data_ptr,
                                                   int data_len) {
  if (data_len < mmt_header_t::len())
    return pmt::get_PMT_NIL();
  mmt_header_t mmt;
  mmt.load(data_ptr);
  data_ptr += mmt_header_t::len();
  data_len -= mmt_header_t::len();
  assert(data_len >= 0);

  eth_header_t eth;
  const int eth_zip_len = eth.eth_zip_load(mmt, data_ptr, data_len);
  if (eth_zip_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += eth_zip_len;
  data_len -= eth_zip_len;
  assert(data_len >= 0);
  const int eth_raw_len = eth.eth_raw_len();
  assert(eth_raw_len >= 0);

  ip4_header_t ip4;
  const int ip4_zip_len = ip4.ip4_zip_load1(mmt, data_ptr, data_len);
  if (ip4_zip_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += ip4_zip_len;
  data_len -= ip4_zip_len;
  assert(data_len >= 0);
  const int ip4_raw_len = ip4.ip4_raw_len();
  assert(ip4_raw_len >= 0);

  udp_header_t udp;
  int udp_raw_len = 0;
  if (ip4.prot == 0x11 && (ip4.frag & 0x1fff) == 0) {
    const int udp_zip_len = udp_header_t::udp_zip_len(mmt);
    if (udp_zip_len < 0 || data_len < udp_zip_len)
      return pmt::get_PMT_NIL();
    udp.udp_zip_load1(mmt, data_ptr);
    data_ptr += udp_zip_len;
    data_len -= udp_zip_len;
    assert(data_len >= 0);
    udp_raw_len = udp_header_t::udp_raw_len();
    assert(udp_raw_len >= 0);
  }

  tcp_header_t tcp;
  int tcp_raw_len = 0;
  if (ip4.prot == 0x06 && (ip4.frag & 0x1fff) == 0) {
    const int tcp_zip_len = tcp.tcp_zip_load1(mmt, data_ptr, data_len);
    if (tcp_zip_len < 0)
      return pmt::get_PMT_NIL();
    data_ptr += tcp_zip_len;
    data_len -= tcp_zip_len;
    assert(data_len >= 0);
    tcp_raw_len = tcp.tcp_raw_len(); // does not depend on tcp.zip.load2
    assert(tcp_raw_len >= 0);
  }

  mgn_header_t mgn; // we do not depend on ip4_zip_load2
  const int mgn_zip_len = mgn.mgn_zip_load(mmt, ip4, data_ptr, data_len);
  if (mgn_zip_len < 0)
    return pmt::get_PMT_NIL();
  data_ptr += mgn_zip_len;
  data_len -= mgn_zip_len;
  assert(data_len >= 0);
  const int mgn_raw_len = mgn.mgn_raw_len();
  assert(mgn_raw_len >= 0);

  // finish loading ip4
  ip4.ip4_zip_load2(mmt, udp_raw_len + tcp_raw_len + mgn_raw_len + data_len);

  assert(data_len >= 0);
  pmt::pmt_t data2 =
      pmt::make_u8vector(eth_raw_len + ip4_raw_len + udp_raw_len + tcp_raw_len +
                             mgn_raw_len + data_len,
                         0);
  size_t data2_len = 0;
  uint8_t *data2_ptr = pmt::u8vector_writable_elements(data2, data2_len);

  std::memcpy(data2_ptr + eth_raw_len + ip4_raw_len + udp_raw_len +
                  tcp_raw_len + mgn_raw_len,
              data_ptr, data_len);

  mgn.mgn_raw_save(ip4, mmt.flowid,
                   data2_ptr + eth_raw_len + ip4_raw_len + udp_raw_len +
                       tcp_raw_len);

  if (ip4.prot == 0x11 && (ip4.frag & 0x1fff) == 0) {
    assert(tcp_raw_len == 0);
    // finish loading udp
    udp.udp_zip_load2(mmt, ip4,
                      data2_ptr + eth_raw_len + ip4_raw_len + udp_raw_len,
                      mgn_raw_len + data_len);
    udp.udp_raw_save(data2_ptr + eth_raw_len + ip4_raw_len);
  }

  if (ip4.prot == 0x06 && (ip4.frag & 0x1fff) == 0) {
    assert(udp_raw_len == 0);
    // finish loading tcp
    tcp.tcp_zip_load2(mmt, ip4,
                      data2_ptr + eth_raw_len + ip4_raw_len + tcp_raw_len,
                      mgn_raw_len + data_len);
    tcp.tcp_raw_save(data2_ptr + eth_raw_len + ip4_raw_len);
  }

  ip4.ip4_raw_save(data2_ptr + eth_raw_len);
  eth.eth_raw_save(data2_ptr);

  if (mgn_raw_len > 0)
    mgen_seqno = mgn.mgenSeqno;
  process_header(mmt, data2_ptr, data2_len);
  return data2;
}

void header_decompressor_impl::process_header(const mmt_header_t &mmt,
                                              const uint8_t *raw_ptr,
                                              int raw_len) {
  if (print_headers && observed_headers.insert(mmt).second) {
    std::stringstream str;
    str << alias() << ": new mmt " << mmt << " seqno " << mgen_seqno
        << " packet 0x" << std::right << std::setfill('0') << std::hex;
    for (int i = 0; i < raw_len; i++)
      str << std::setw(2) << (unsigned int)raw_ptr[i];
    str << std::endl;
    std::cout << str.str();
  }
}

void header_decompressor_impl::collect_block_report(ModemReport *modem_report) {
  HeaderCompression *report = modem_report->mutable_header_decompressor();
  gr::thread::scoped_lock lock(mutex);

  report->set_valid_messages(valid_messages);
  report->set_invalid_messages(invalid_messages);
  report->set_uncompressed_bytes(uncompressed_bytes);
  report->set_compressed_bytes(compressed_bytes);

  valid_messages = 0;
  invalid_messages = 0;
  uncompressed_bytes = 0;
  compressed_bytes = 0;
}

} /* namespace marmote3 */
} /* namespace gr */
