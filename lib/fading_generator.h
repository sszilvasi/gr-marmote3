/* -*- c++ -*- */
/*
 * Copyright 2017 Peter Horvath.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef FADING_GENERATOR_H
#define FADING_GENERATOR_H

#include <gnuradio/gr_complex.h>

namespace gr {

namespace marmote3 {

enum DOPPLER_SPECTRUM { Jakes = 0 };

enum FADING_TYPE { Independent, Correlated };

enum CHANNEL_PROFILE {
  ITU_Vehicular_A,
  ITU_Vehicular_B,
  ITU_Pedestrian_A,
  ITU_Pedestrian_B
};

class fading_generator {
public:
  fading_generator() {}
  virtual ~fading_generator(){};

private:
};

class fading_generator_independent : public fading_generator {
public:
  fading_generator_independent() : fading_generator() {}
  // fading_generator_independent(const fading_generator_independent &) {}
  virtual ~fading_generator_independent() {}

  // using fading_generator::generate;
  // virtual std::vector<gr_complex> generate(int no_samples) = 0;

  //! Generate \a no_samples values from the fading process
  // virtual std::vector<gr_complex> generate(int no_samples) {}

  virtual void set_time_offset(int offset) {}
};

class fading_generator_correlated : public fading_generator {
public:
  fading_generator_correlated(double norm_doppler, DOPPLER_SPECTRUM spectrum);
  virtual ~fading_generator_correlated();

  virtual void set_norm_doppler(double norm_doppler);

  virtual void set_doppler_spectrum(DOPPLER_SPECTRUM spectrum);
  
  //! Set relative Doppler of the LOS component
  // virtual void set_LOS_doppler(double relative_doppler);

  //! Get relative Doppler of the LOS component
  // virtual double get_LOS_doppler() const = 0;

  //! Shift generator time offset by a number of samples
  virtual void shift_time_offset(int no_samples) {
    time_offset += static_cast<double>(no_samples);
  }

  //! Set relative LOS power
  virtual void set_K_factor(double K_factor);
 
  //! Get relative power of LOS component (Rice factor)
  virtual double get_K_factor() const { return K_factor; }
  
  //! Get time offset in samples
  virtual double get_time_offset() const { return time_offset; }
  //! Return normalized Doppler
  virtual double get_norm_doppler() const { return norm_doppler; }
  //! Return Doppler spectrum (for Rice fading generator)
  virtual DOPPLER_SPECTRUM get_doppler_spectrum() const {
    return doppler_spectrum;
  }

  virtual void generate(gr_complex *samples, int no_samples) = 0;

protected:
  double time_offset; //!< Time offset in samples (time state in the generator)
  double norm_doppler;
  DOPPLER_SPECTRUM doppler_spectrum;
  double K_factor; 
  float diffuse_power;
  float los_power;
};

class fading_generator_sos : public fading_generator_correlated {
public:
  static const int N = 12;

  fading_generator_sos(double norm_doppler, DOPPLER_SPECTRUM spectrum);
  ~fading_generator_sos();

  virtual void generate(gr_complex *samples, int no_samples);
  virtual void shift_time_offset(int no_samples);

private:
  float phases_i[N], phases_q[N];
  float freqs_i[N], freqs_q[N];

  const float nf;
};

} // namespace marmote3
} // namespace sr
#endif
