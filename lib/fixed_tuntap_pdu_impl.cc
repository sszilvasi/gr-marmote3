/* -*- c++ -*- */
/*
 * Copyright 2013 Free Software Foundation, Inc.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "fixed_tuntap_pdu_impl.h"
#include <gnuradio/io_signature.h>
#include <sstream>
#include <stdexcept>

#ifndef __APPLE__
#include <arpa/inet.h>
#include <fcntl.h>
#include <linux/if.h>
#include <linux/if_tun.h>
#include <sys/ioctl.h>
#endif //__APPLE__

namespace gr {
namespace marmote3 {

#ifndef __APPLE__

fixed_tuntap_pdu::sptr fixed_tuntap_pdu::make(const std::string &dev, int mtu,
                                              bool istunflag) {
  return gnuradio::get_initial_sptr(
      new fixed_tuntap_pdu_impl(dev, mtu, istunflag));
}

fixed_tuntap_pdu_impl::fixed_tuntap_pdu_impl(const std::string &dev, int mtu,
                                             bool istunflag)
    : gr::block("fixed_tuntap_pdu", gr::io_signature::make(0, 0, 0),
                gr::io_signature::make(0, 0, 0)),
      in_port(pmt::mp("in")), out_port(pmt::mp("out")),
      frame_len(istunflag ? mtu : mtu + 14), fd(-1) {
  // make the tuntap
  char dev_cstr[1024];
  memset(dev_cstr, 0x00, 1024);
  strncpy(dev_cstr, dev.c_str(), std::min((size_t)1023, dev.size()));

  fd = tun_alloc(dev_cstr,
                 istunflag ? (IFF_TUN | IFF_NO_PI) : (IFF_TAP | IFF_NO_PI));
  if (fd <= 0)
    throw std::runtime_error(
        "fixed_tuntap_pdu: failed to allocate device (are you root?)");

  int err = set_mtu(dev_cstr, mtu);
  if (err < 0)
    std::cout << "fixed_tuntap_pdu: failed to set MTU (use ifconfig)\n";

  std::cout << "fixed_tuntap_pdu: opened interface " << dev_cstr << std::endl;

  message_port_register_out(out_port);
  thread = gr::thread::thread(boost::bind(&fixed_tuntap_pdu_impl::work, this));

  message_port_register_in(in_port);
  set_msg_handler(in_port, boost::bind(&fixed_tuntap_pdu_impl::send, this, _1));
}

fixed_tuntap_pdu_impl::~fixed_tuntap_pdu_impl() {
  std::cout << "fixed_tuntap_pdu: trying to stop worker\n";

  thread.interrupt();
  thread.join();
}

void fixed_tuntap_pdu_impl::send(pmt::pmt_t message) {
  if (!pmt::is_pair(message) || !pmt::is_uniform_vector(pmt::cdr(message)))
    std::cout << "fixed_tuntap_pdu: invalid message format\n";
  else {
    pmt::pmt_t vec = pmt::cdr(message);

    size_t len(0);
    pmt::uniform_vector_elements(vec, len);
    if (len > frame_len)
      std::cout << "fixed_tuntap_pdf: message is too long\n";
    else {
      size_t offset(0);
      int r = write(fd, pmt::uniform_vector_elements(vec, offset), len);
      if (r != len)
        std::cout << "fixed_tuntap_pdf: write operation failed\n";

      if (false) {
        std::stringstream msg;
        msg << "fixed_tuntap_pdf: wrote " << r << " byte packet\n";
        std::cout << msg.str();
      }
    }
  }
}

void fixed_tuntap_pdu_impl::work() {
  std::vector<uint8_t> buffer(frame_len);

  while (!boost::this_thread::interruption_requested()) {
    // setup timeval for timeout
    timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100000; // 100 ms

    // setup rset for timeout
    fd_set rset;
    FD_ZERO(&rset);
    FD_SET(fd, &rset);

    // call select with timeout on receive socket
    int r = select(fd + 1, &rset, NULL, NULL, &tv);
    if (r <= 0)
      continue;

    r = read(fd, buffer.data(), buffer.size());
    if (r <= 0)
      std::cout << "fixed_tuntap_pdf: read operation failed\n";

    if (false) {
      std::stringstream msg;
      msg << "fixed_tuntap_pdf: read " << r << " byte packet\n";
      std::cout << msg.str();
    }

    pmt::pmt_t vec = pmt::init_u8vector(r, buffer);
    pmt::pmt_t pdu = pmt::cons(pmt::PMT_NIL, vec);

    message_port_pub(out_port, pdu);
  }

  std::cout << "fixed_tuntap_pdu: worker has stopped\n";
}

int fixed_tuntap_pdu_impl::tun_alloc(char *dev, int flags) {
  struct ifreq ifr;
  int fd, err;
  const char *clonedev = "/dev/net/tun";

  /* Arguments taken by the function:
   *
   * char *dev: the name of an interface (or '\0'). MUST have enough
   *   space to hold the interface name if '\0' is passed
   * int flags: interface flags (eg, IFF_TUN etc.)
   */

  /* open the clone device */
  if ((fd = open(clonedev, O_RDWR)) < 0)
    return fd;

  /* preparation of the struct ifr, of type "struct ifreq" */
  memset(&ifr, 0, sizeof(ifr));

  ifr.ifr_flags = flags; /* IFF_TUN or IFF_TAP, plus maybe IFF_NO_PI */

  /* if a device name was specified, put it in the structure; otherwise,
   * the kernel will try to allocate the "next" device of the
   * specified type
   */
  if (*dev)
    strncpy(ifr.ifr_name, dev, IFNAMSIZ - 1);

  /* try to create the device */
  if ((err = ioctl(fd, TUNSETIFF, (void *)&ifr)) < 0) {
    close(fd);
    return err;
  }

  /* if the operation was successful, write back the name of the
   * interface to the variable "dev", so the caller can know
   * it. Note that the caller MUST reserve space in *dev (see calling
   * code below)
   */
  strcpy(dev, ifr.ifr_name);

  /* this is the special file descriptor that the caller will use to talk
   * with the virtual interface
   */
  return fd;
}

int fixed_tuntap_pdu_impl::set_mtu(const char *dev, int mtu) {
  struct ifreq ifr;
  int sfd, err;

  /* MTU must be set by passing a socket fd to ioctl;
   * create an arbitrary socket for this purpose
   */
  if ((sfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    return sfd;

  /* preparation of the struct ifr, of type "struct ifreq" */
  memset(&ifr, 0, sizeof(ifr));
  strncpy(ifr.ifr_name, dev, IFNAMSIZ);
  ifr.ifr_addr.sa_family = AF_INET; /* address family */
  ifr.ifr_mtu = mtu;

  /* try to set MTU */
  if ((err = ioctl(sfd, SIOCSIFMTU, (void *)&ifr)) < 0)
    return err;

  return mtu;
}

#else //__APPLE__

fixed_tuntap_pdu::sptr fixed_tuntap_pdu::make(const std::string &dev, int mtu,
                                              bool istunflag) {
  throw std::domain_error("fixed_tuntap_pdu: not implemented on OSX");
}

#endif //__APPLE__

} /* namespace marmote3 */
} /* namespace gr */
