/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "subcarrier_remapper_impl.h"
#include <gnuradio/io_signature.h>

namespace gr {
namespace marmote3 {

subcarrier_remapper::sptr subcarrier_remapper::make(int vlen,
                                                    const std::vector<int> &map,
                                                    int output_multiple) {
  return gnuradio::get_initial_sptr(
      new subcarrier_remapper_impl(vlen, map, output_multiple));
}

subcarrier_remapper_impl::subcarrier_remapper_impl(int vlen,
                                                   const std::vector<int> &map,
                                                   int output_multiple)
    : gr::sync_block(
          "subcarrier_remapper",
          gr::io_signature::make(1, 1, sizeof(gr_complex) * vlen),
          gr::io_signature::make(1, 1, sizeof(gr_complex) * map.size())),
      vlen(vlen), map(map) {
  if (vlen <= 0 || map.size() <= 0)
    throw std::invalid_argument(
        "subcarrier_remapper: vector lengths must be positive");

  for (int i = 0; i < map.size(); i++)
    if (map[i] < -1 || map[i] >= vlen)
      throw std::invalid_argument("subcarrier_remapper: invalid map entry");

  if (output_multiple < 0)
    throw std::invalid_argument("subcarrier_remapper: invalid output_multiple");
  else if (output_multiple >= 1)
    set_output_multiple(output_multiple);
}

subcarrier_remapper_impl::~subcarrier_remapper_impl() {}

int subcarrier_remapper_impl::work(int noutput_items,
                                   gr_vector_const_void_star &input_items,
                                   gr_vector_void_star &output_items) {
  const gr_complex *in = (const gr_complex *)input_items[0];
  gr_complex *out = (gr_complex *)output_items[0];

  gr_complex zero(0.0f, 0.0f);
  for (int n = 0; n < noutput_items; n++) {
    for (int i = 0; i < map.size(); i++)
      *(out++) = map[i] < 0 ? zero : in[map[i]];

    in += vlen;
  }

  return noutput_items;
}

} /* namespace marmote3 */
} /* namespace gr */
