/* -*- c++ -*- */
/*
 * Copyright 2019 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "radar_detector_impl.h"
#include <gnuradio/io_signature.h>
#include <limits>
#include <marmote3/marmote3.pb.h>
#include <pmmintrin.h>
#include <smmintrin.h>

namespace gr {
namespace marmote3 {

radar_detector::sptr radar_detector::make(int vlen, float alpha, float power,
                                          int min_count, int max_report,
                                          bool debug) {
  return gnuradio::get_initial_sptr(new radar_detector_impl(
      vlen, alpha, power, min_count, max_report, debug));
}

radar_detector_impl::radar_detector_impl(int vlen, float alpha, float power,
                                         int min_count, int max_report,
                                         bool debug)
    : gr::sync_block("radar_detector",
                     gr::io_signature::make(1, 1, vlen * sizeof(float)),
                     gr::io_signature::make(0, 0, 0)),
      vlen(vlen), alpha(alpha), power(power), min_count(min_count),
      max_report(max_report), debug(debug) {
  if (alpha <= 0.0f || alpha > 1.0f)
    throw std::invalid_argument("radar_detector: invalid alpha value");

  if (power <= 0.0f)
    throw std::invalid_argument("radar_detector: power must be positive");

  if (vlen <= 0 || vlen % 16 != 0)
    throw std::invalid_argument("radar_detector: vlen must be divisible by 16");

  if (min_count < 0)
    throw std::invalid_argument("radar_detector: min count cannot be negative");

  if (max_report < 0)
    throw std::invalid_argument(
        "radar_detector: max report cannot be negative");

  sse_stats = new sse_stat_t[vlen / 16];
  std::memset(sse_stats, 0, sizeof(sse_stat_t) * (vlen / 16));

  cpu_stats = new cpu_stat_t[vlen];
  std::memset(cpu_stats, 0, sizeof(cpu_stat_t) * vlen);

  min_level = 0.0f;
  report_start = 0;
  report_count = 0;
}

radar_detector_impl::~radar_detector_impl() {
  delete[] sse_stats;
  sse_stats = NULL;

  delete[] cpu_stats;
  cpu_stats = NULL;
}

int radar_detector_impl::sse_work(const float *in) {
  __m128 bar = _mm_set_ps1(power * alpha * min_level);
  __m128 beta = _mm_set_ps1(1.0f - alpha);

  __m128 minimum = _mm_set_ps1(std::numeric_limits<float>::max());
  int count = 0;

  for (int i = 0; i < vlen / 16; i += 1) {
    sse_stat_t &stat = sse_stats[i];

    __m128 data0 = _mm_load_ps(in);
    __m128 data1 = _mm_load_ps(in + 4);
    __m128 data2 = _mm_load_ps(in + 8);
    __m128 data3 = _mm_load_ps(in + 12);
    in += 16;

    __m128 level0 = _mm_add_ps(_mm_mul_ps(stat.level0, beta), data0);
    __m128 level1 = _mm_add_ps(_mm_mul_ps(stat.level1, beta), data1);
    __m128 level2 = _mm_add_ps(_mm_mul_ps(stat.level2, beta), data2);
    __m128 level3 = _mm_add_ps(_mm_mul_ps(stat.level3, beta), data3);

    minimum = _mm_min_ps(minimum, _mm_min_ps(level0, level1));
    minimum = _mm_min_ps(minimum, _mm_min_ps(level2, level3));

    stat.level0 = level0;
    stat.level1 = level1;
    stat.level2 = level2;
    stat.level3 = level3;

    __m128i sign0 = _mm_castps_si128(_mm_cmpgt_ps(data0, bar));
    __m128i sign1 = _mm_castps_si128(_mm_cmpgt_ps(data1, bar));
    __m128i sign2 = _mm_castps_si128(_mm_cmpgt_ps(data2, bar));
    __m128i sign3 = _mm_castps_si128(_mm_cmpgt_ps(data3, bar));

    __m128i sign = _mm_packs_epi16(_mm_blend_epi16(sign0, sign1, 0xAA),
                                   _mm_blend_epi16(sign2, sign3, 0xAA));
    __m128i above = stat.above;

    __m128i match =
        _mm_andnot_si128(sign, _mm_cmpeq_epi8(above, _mm_set1_epi8(0x08)));
    count += __builtin_popcount(_mm_movemask_epi8(match));

    stat.above = _mm_or_si128(_mm_add_epi8(above, above),
                              _mm_and_si128(sign, _mm_set1_epi8(0x01)));
  }

  // calculate the minimum of the 4 floats
  __m128 shuf = _mm_movehdup_ps(minimum);
  minimum = _mm_min_ps(minimum, shuf);
  shuf = _mm_movehl_ps(shuf, minimum);
  minimum = _mm_min_ss(minimum, shuf);
  min_level = _mm_cvtss_f32(minimum);

  return count;
}

int radar_detector_impl::cpu_work(const float *in) {
  float bar = power * alpha * min_level;
  float beta = 1.0f - alpha;

  min_level = std::numeric_limits<float>::max();
  int count = 0;

  for (int i = 0; i < vlen; i++) {
    cpu_stat_t &stat = cpu_stats[i];
    float data = in[i];

    stat.level = beta * stat.level + data;
    min_level = std::min(min_level, stat.level);

    int above = (int(stat.above) << 1) | (data > bar ? 1 : 0);
    if (above == 0x010)
      count += 1;
    stat.above = above;
  }

  return count;
}

int radar_detector_impl::work(int noutput_items,
                              gr_vector_const_void_star &input_items,
                              gr_vector_void_star &output_items) {
  const float *in = (const float *)(input_items[0]);

  for (int n = 0; n < noutput_items; n++) {
    int count;
    if (true)
      count = sse_work(in);
    else
      count = cpu_work(in);

    if (last_count + count >= min_count && count < min_count)
      temp_items.emplace_back(n, last_count + count);

    last_count = count;
    in += vlen;
  }

  // update atomically
  gr::thread::scoped_lock lock(mutex);

  if (!temp_items.empty() && report_items.size() < max_report) {
    unsigned int base = nitems_read(0) - report_start;
    for (const item_t &item : temp_items)
      report_items.emplace_back(base + item.offset, item.magnitude);
  }

  temp_items.clear();
  report_count += noutput_items;

  return noutput_items;
}

void radar_detector_impl::collect_block_report(ModemReport *modem_report) {
  RadarDetectorReport *report = modem_report->mutable_radar_detector();

  gr::thread::scoped_lock lock(mutex);
  report->set_start(report_start);
  report->set_count(report_count);

  for (const item_t &item : report_items) {
    RadarDetectorItem *item2 = report->add_items();
    item2->set_offset(item.offset);
    item2->set_magnitude(item.magnitude);
  }
  report_items.clear();

  report_start = nitems_read(0);
  report_count = 0;

  if (debug) {
    int prev_offset = 0;
    for (const RadarDetectorItem &item : report->items()) {
      std::cout << item.offset() << " " << item.magnitude() << " "
                << (item.offset() - prev_offset) << std::endl;
      prev_offset = item.offset();
    }
  }
}

} /* namespace marmote3 */
} /* namespace gr */
