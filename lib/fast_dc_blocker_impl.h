/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_FAST_DC_BLOCKER_IMPL_H
#define INCLUDED_MARMOTE3_FAST_DC_BLOCKER_IMPL_H

#include <marmote3/fast_dc_blocker.h>

namespace gr {
namespace marmote3 {

class fast_dc_blocker_impl : public fast_dc_blocker {
private:
  const int length;
  const float scale;

  int pos;
  gr_complex *delay;
  gr_complex sum1, sum2;

public:
  fast_dc_blocker_impl(int length);
  ~fast_dc_blocker_impl();

  int work(int noutput_items, gr_vector_const_void_star &input_items,
           gr_vector_void_star &output_items);
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_FAST_DC_BLOCKER_IMPL_H */
