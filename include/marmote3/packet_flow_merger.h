/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_PACKET_FLOW_MERGER_H
#define INCLUDED_MARMOTE3_PACKET_FLOW_MERGER_H

#include <gnuradio/block.h>
#include <marmote3/api.h>
#include <marmote3/modem_monitor.h>

namespace gr {
namespace marmote3 {

/*!
 * \brief <+description of block+>
 * \ingroup marmote3
 */
class MARMOTE3_API packet_flow_merger : virtual public gr::block,
                                        virtual public modem_monitored {
public:
  typedef boost::shared_ptr<packet_flow_merger> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of
   * marmote3::packet_flow_merger.
   *
   * To avoid accidental use of raw pointers, marmote3::packet_flow_merger's
   * constructor is in a private implementation
   * class. marmote3::packet_flow_merger::make is the public interface for
   * creating new instances.
   */
  static sptr make(int itemsize, int nflows, int max_output_len);

  gr::block *get_this_block() override { return this; }
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_PACKET_FLOW_MERGER_H */
