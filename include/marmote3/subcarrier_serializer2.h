/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_SUBCARRIER_SERIALIZER2_H
#define INCLUDED_MARMOTE3_SUBCARRIER_SERIALIZER2_H

#include <gnuradio/block.h>
#include <marmote3/api.h>
#include <marmote3/modem_monitor.h>

namespace gr {
namespace marmote3 {

/*!
 * \brief <+description of block+>
 * \ingroup marmote3
 */
class MARMOTE3_API subcarrier_serializer2 : virtual public gr::block,
                                            virtual public modem_monitored {
public:
  typedef boost::shared_ptr<subcarrier_serializer2> sptr;

  /*!
   * \brief Return a shared_ptr to a new instance of
   * marmote3::subcarrier_serializer2.
   *
   * To avoid accidental use of raw pointers, marmote3::subcarrier_serializer2's
   * constructor is in a private implementation
   * class. marmote3::subcarrier_serializer2::make is the public interface for
   * creating new instances.
   */
  static sptr make(int vlen, int trig_rate, float trig_level, int preamb_offset,
                   int postamb_offset, bool postamble, int min_frame_len,
                   int max_frame_len, const std::vector<int> &enabled);

  virtual void set_enabled(const std::vector<int> &enabled) = 0;
  virtual std::vector<int> get_enabled() = 0;

  gr::block *get_this_block() override { return this; }
};

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_SUBCARRIER_SERIALIZER2_H */
