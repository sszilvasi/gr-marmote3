/* -*- c++ -*- */
/*
 * Copyright 2017 Miklos Maroti.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_MARMOTE3_CONSTANTS_H
#define INCLUDED_MARMOTE3_CONSTANTS_H

#include <cmath>
#include <gnuradio/gr_complex.h>
#include <marmote3/api.h>
#include <pmt/pmt.h>
#include <vector>

namespace gr {
namespace marmote3 {

MARMOTE3_API extern const pmt::pmt_t PMT_NULL;
MARMOTE3_API extern const pmt::pmt_t PMT_PACKET_LEN;
MARMOTE3_API extern const pmt::pmt_t PMT_PACKET_DICT;
MARMOTE3_API extern const pmt::pmt_t PMT_EMPTY_DICT;
MARMOTE3_API extern const pmt::pmt_t PMT_PACKET_SRC;
MARMOTE3_API extern const pmt::pmt_t PMT_PACKET_DST;
MARMOTE3_API extern const pmt::pmt_t PMT_PACKET_ID;
MARMOTE3_API extern const pmt::pmt_t PMT_PACKET_MCS;
MARMOTE3_API extern const pmt::pmt_t PMT_DATA_LEN;
MARMOTE3_API extern const pmt::pmt_t PMT_CHANNEL_NUM;
MARMOTE3_API extern const pmt::pmt_t PMT_TRIG_SAMP;
MARMOTE3_API extern const pmt::pmt_t PMT_TRIG_TIME;
MARMOTE3_API extern const pmt::pmt_t PMT_PREAMB_FREQ;
MARMOTE3_API extern const pmt::pmt_t PMT_FREQ_ERR;
MARMOTE3_API extern const pmt::pmt_t PMT_PHASE_ERR;
MARMOTE3_API extern const pmt::pmt_t PMT_EQU_TAPS;
MARMOTE3_API extern const pmt::pmt_t PMT_EQU_PWR;
MARMOTE3_API extern const pmt::pmt_t PMT_EQU_SNR;
MARMOTE3_API extern const pmt::pmt_t PMT_LOAD_TIME;
MARMOTE3_API extern const pmt::pmt_t PMT_SCHD_TIME;
MARMOTE3_API extern const pmt::pmt_t PMT_FLOW_ID;

enum modulation_t {
  MOD_MCS = -1, // get modulation from MCS
  MOD_BPSK = 1,
  MOD_QPSK = 2,
  MOD_8PSK = 3,
  MOD_16QAM = 4,
  MOD_64QAM = 6,
};

MARMOTE3_API int get_modulation_order(modulation_t mod);

enum header_t {
  HDR_NONE = 0,
  HDR_SRC_MCS_LEN_CRC, // 32-bit, 40-symb
};

struct MARMOTE3_API mcscheme_t {
  modulation_t mod;
  float rate;      // between 0.0 and 1.0
  float ext_power; // tx power adjustment in dB

  int get_code_len(int data_len) const;
  int get_symb_len(int data_len) const;
};

const int MCSCHEME_NUM = 12;
MARMOTE3_API const mcscheme_t &get_mcscheme(int index);

} // namespace marmote3
} // namespace gr

#endif /* INCLUDED_MARMOTE3_CONSTANTS_H */
