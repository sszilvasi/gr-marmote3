title: gr-marmote3
brief: The modem of the MarmotE team that was used in the DARPA SC2 competition
tags: # Tags are arbitrary, but look at CGRAN what other authors are using
  - sdr
author:
  - Miklos Maroti <mmaroti@gmail.com>
copyright_owner:
  - Miklos Maroti
license: Copyright (C) MarmotE, all rights reserved.
#repo: 
#website: <module_website> # If you have a separate project website, put it here
#icon: <icon_url> # Put a URL to a square image here that will be used as an icon on CGRAN
---
